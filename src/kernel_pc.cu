//============================================================================
// Name        : kernel_pc.cu
// Authors     : Andres Diaz - Sixto Campaña - Eduardo Caicedo
// Version     : 1.1
// Copyright   : All rights reserved
// Description : AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1
//============================================================================

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <limits>

#include <math.h>

#include <cuda_runtime.h>
#include </usr/local/cuda-10.0/samples/common/inc/helper_math.h>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include "kernel_pc.h"

using namespace std;
using namespace cv;


/// Number of threads per block
#define NTHREADS_BLOCK 16

/**
 * @brief kernel_pc::kernel_pc constructor of the class kernel_pc
 */
kernel_pc::kernel_pc(void){
}

/**
 * @brief kernel_pc::~kernel_pc destructor of the class kernel_pc
 */
kernel_pc::~kernel_pc(){
}

/**
 * @brief divUpN devides size image between number of threads
 * @param num number of columns or rows of an image
 * @param den number of threads, for example, nThreadsP.x (for columns) or nThreadsP.y (for rows)
 * @return number of threads to be used in a given task
 */
inline int divUpN(int num, int den) {
    return ((num+den-1)/den);
}

texture<float, 2> texDepthRC;/**< Texture memory for the depth map */
texture<float4, 2> texVertK;/**< Texture memory for local vertices */

__constant__ float k_const6[9];/**< constant memory for the intrinsic matrix */


//Initializes the variables, allocates texture memory, host memory and device memory for the data
void kernel_pc::InitPcGPU(Mat K, int Dcols, int Drows, float Dsize_m_x, float Dsize_m_z, float Dcell_m, int Dmax_iter_ransac, float Dth_ransac, float Dth_occupancy_grid){

    texDepthRC.filterMode = cudaFilterModeLinear;//defines the mode of the texture memory
    texVertK.filterMode = cudaFilterModePoint;

    int cols=Dcols;//assigns number of columns and rows of the images
    int rows=Drows;

    min_range=0; //lower limit for computing random numbers for RANSAC
    max_iter_ransac=Dmax_iter_ransac;//maximum  number of iterations for RANSAC
    th_ransac=Dth_ransac; //threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]

    size_m_x=Dsize_m_x;//size of the grid in x direction, in [m]
    size_m_z=Dsize_m_z;//size of the grid in z direction (y axis is parallel to gravity vector), in [m]
    cell_m=Dcell_m;//long of a squared cell that belongs to the grid, in [m]
    size_c_x=size_m_x/cell_m;//number of cells of the grid in x direction
    size_c_z=size_m_z/cell_m;//number of cells of the grid in z direction

    th_outlier_y=3.0; //threshold that defines if a point is an outlier by evaluating its y coordinate, in [m]
    th_normal_y=0.8; //threshold that defines if a normalized normal vector is parallel to the gravity vector //0.85
    part_max_val=0.7; //part of the maximum value in y coordinate that defines if a point is close to the floor //0.6
    min_dis_user=0.4; //minimum distance over which the occupancy 2D grid is updated, in [m]
    th_occupancy_grid=Dth_occupancy_grid; //threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m]

    float *K_host = (float*) K.data;//intrinsic matrix of the camera ZED Mini
    cudaMemcpyToSymbol(k_const6, K_host, 3 * 3 * sizeof (float));//initializes constant memory

    verticesW_host=(Vertex*)malloc(cols*rows*sizeof(Vertex));//global vertices in host memory
    normalsW_host=(Normal*)malloc(cols*rows*sizeof(Normal));//global normal vectors in host memory

    compactedDataId_host=(uint*)malloc(cols*rows*sizeof(uint));//indices of points for an initial segmentation

    randNum_host=(int*)malloc(3*max_iter_ransac*sizeof(int));//random numbers for RANSAC

    OccuO_host=(uint*)malloc((int) size_c_x * size_c_z * sizeof(uint));//allocates host memory for a grid that stores the number of occupied points proyected over each cell.
    OccuF_host=(uint*)malloc((int) size_c_x * size_c_z * sizeof(uint));//allocates host memory for a grid that stores the number of free points proyected over each cell.
    OccuOW_host=(uint*)malloc((int) size_c_x * size_c_z * sizeof(uint));//allocates host memory for a global grid that identifies cells that are occupied
    OccuFW_host=(uint*)malloc((int) size_c_x * size_c_z * sizeof(uint));//allocates host memory for a global grid that identifies cells that are free

    reactnav_host=(uint*)malloc(6 * sizeof(uint));//allocates host memory for occupied cells inside the semicircle, used for reactive navigation

    verticesWnew_host=(Vertex*)malloc(cols*rows*sizeof(Vertex));//global vertices in host memory, for the next iteration

    size_t bytes_per_row = sizeof (float)*cols; //bytes per row for float data

    size_t pitch_v;
    size_t bytes_per_row_v = sizeof (Vertex)*cols;//bytes per row for Vertex data

    //allocates memory in device
    cudaMallocPitch(&depth_gpu, &pitch2, bytes_per_row, rows);
    cudaMallocPitch( &verticesK_gpu, &pitch_v, bytes_per_row_v, rows );

    //mapps pointers to textures
    cudaBindTexture2D(NULL, texDepthRC, depth_gpu, chd_float2, cols, rows, pitch2);//depth.step  //672x376  2688
    cudaBindTexture2D(NULL, texVertK, verticesK_gpu, chd_float4, cols, rows, pitch_v);

    size_t pitch_n;
    size_t bytes_per_row_n = sizeof (Normal)*cols;//bytes per row for Normal data

    //allocates memory in device
    cudaMallocPitch( &verticesW_gpu, &pitch_v, bytes_per_row_v, rows );
    cudaMallocPitch( &normalsW_gpu, &pitch_n, bytes_per_row_n, rows );

    //allocates memory in device
    cudaMalloc( (void**)&verticesY_gpu, cols*rows * sizeof(float) );
    cudaMalloc( (void**)&verFlags_gpu, cols*rows * sizeof(uint) );
    cudaMalloc( (void**)&verFlagsScan_gpu, cols*rows * sizeof(uint) );
    cudaMalloc( (void**)&compactedDataId_gpu, cols*rows * sizeof(uint) );

    cudaMalloc( (void**)&randNum_gpu, 3 * max_iter_ransac * sizeof(int) );
    cudaMalloc( (void**)&diff_lth_gpu, cols * rows * max_iter_ransac * sizeof(uint) );

    //allocates memory in device
    size_t pitch_v2;
    cudaMallocPitch( &verticesWnew_gpu, &pitch_v2, bytes_per_row_v, rows );

    //allocates memory in device
    cudaMalloc( (void**)&OccuF_gpu, (int) size_c_x * size_c_z * sizeof(uint) );
    cudaMalloc( (void**)&OccuO_gpu, (int) size_c_x * size_c_z * sizeof(uint) );
    cudaMalloc( (void**)&OccuFW_gpu, (int) size_c_x * size_c_z * sizeof(uint) );
    cudaMalloc( (void**)&OccuOW_gpu, (int) size_c_x * size_c_z * sizeof(uint) );

    //allocates memory in device
    cudaMalloc( (void**)&reactnav_gpu, (int) 6 * sizeof(uint) );	//six intervals 0-30, 30-60, 60-90, 90-120, 120-150, 150-180
}


/**
 * @brief my_kernelVK computes on GPU local vertices for step k
 * @param verticesK_gpu computed local vertices for step k
 * @param cols number of columns of the image
 * @param rows number of rows of the image
 */
__global__ void my_kernelVK(kernel_pc::Vertex *verticesK_gpu, int cols, int rows){

    int u = blockDim.x * blockIdx.x + threadIdx.x; //index in u direction (horizontal)
    int v = blockDim.y * blockIdx.y + threadIdx.y; //index in v direction (vertical)

    if(u < cols && v < rows){//if indices u and v are in the range of the image
        float tx = u+0.5f;
        float ty = v+0.5f;
        int pixel = u + v*cols;//index for a 1D array

        float fx, fy, cx, cy;//intrinsic parameters of the camera ZED Mini
        fx = k_const6[0];//focal length in x direction
        cx = k_const6[2];//optical center in x direction
        fy = k_const6[4];//focal length in y direction
        cy = k_const6[5];//optical center in y direction

        float depthRC = tex2D(texDepthRC, tx, ty);//reads texture memory for getting depth data from camera ZED Mini
        if(isnan(depthRC) || isinf(depthRC) || depthRC>3 || depthRC<0.4){
            depthRC=0;
        }

        float3 urn_h = make_float3((u - cx)/fx, (v - cy)/fy, 1.0f);//computes normalized coordinates (x/z, y/z, 1)

        //computes local vertices for step k
        verticesK_gpu[pixel].x= urn_h.x*depthRC;//local x coordinate
        verticesK_gpu[pixel].y= urn_h.y*depthRC;//local y coordinate
        verticesK_gpu[pixel].z= urn_h.z*depthRC;//local z coordinate
        verticesK_gpu[pixel].w= depthRC;//local z coordinate
    }
}


//initializes device memory with depth data and calls the kernel my_kernelVK that computes on GPU local vertices for the current iteration
void kernel_pc::GetVertices(Mat depth, int cols, int rows){

    size_t bytes_per_row = sizeof (float)*cols;//bytes per row for float data

    //initializes device memory with depth data
    cudaMemcpy2D(depth_gpu, pitch2, depth.data, depth.step, bytes_per_row, rows, cudaMemcpyHostToDevice);

    // defines the number of threads and blocks (takes advantage of the image structure)
    dim3 nThreadsP(NTHREADS_BLOCK, NTHREADS_BLOCK  , 1);
    dim3 nBlocksP(divUpN(cols, nThreadsP.x), divUpN(rows, nThreadsP.y),1);

    //calls the kernel my_kernelVK
    my_kernelVK<<<nBlocksP,nThreadsP>>>(verticesK_gpu, cols, rows);
    cudaThreadSynchronize();
}


__constant__ float T_const6[12]; /**< constant memory for a transformation matrix (current transformation of the user) */


/**
 * @brief my_kernelVN computes global vertices and global normal vectors using the current transformation of the user
 * @param verticesW_gpu computed global vertices
 * @param normalsW_gpu computes global normal vertices
 * @param verticesY_gpu y component of the coordinates of the points
 * @param cols number of columns of the image
 * @param rows number of rows of the image
 * @param th_outlier_y threshold that defines if a point is an outlier by evaluating its y coordinate, in [m]
 */
__global__ void my_kernelVN(kernel_pc::Vertex *verticesW_gpu, kernel_pc::Normal *normalsW_gpu, float *verticesY_gpu, int cols, int rows, float th_outlier_y){

    int u = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)
    int v = blockDim.y * blockIdx.y + threadIdx.y;//index in v direction (vertical)

    if(u < cols && v < rows){//if indices u and v are in the range of the image
        int pixel = u + v*cols;//index for a 1D array

        verticesW_gpu[pixel].w=0; //fourth component equal to zero
        normalsW_gpu[pixel].w=0; //fourth component equal to zero

        float3 vertK=make_float3(tex2D(texVertK,u,v));//reads local vertices from texture memory
        float3 vertKUp=make_float3(tex2D(texVertK,u+1,v)); //reads vertex on the right of the current vertex
        float3 vertKVp=make_float3(tex2D(texVertK,u,v+1)); //reads vertex below the current vertex

        //global vertices for step k, using current transformation
        verticesW_gpu[pixel].x= T_const6[0]*vertK.x + T_const6[1]*vertK.y + T_const6[2]*vertK.z + T_const6[3];
        verticesW_gpu[pixel].y= T_const6[4]*vertK.x + T_const6[5]*vertK.y + T_const6[6]*vertK.z + T_const6[7];
        verticesW_gpu[pixel].z= T_const6[8]*vertK.x + T_const6[9]*vertK.y + T_const6[10]*vertK.z + T_const6[11];

        float3 difx_vertexk=make_float3(vertKUp.x-vertK.x,vertKUp.y-vertK.y,vertKUp.z-vertK.z); //vectorin x direction
        float3 dify_vertexk=make_float3(vertKVp.x-vertK.x,vertKVp.y-vertK.y,vertKVp.z-vertK.z); //vector in y direction

        float3 normalk = cross(difx_vertexk,dify_vertexk);//normal vector as the cross product between vectorin x direction and vectorin y direction
        float radius = sqrt(normalk.x*normalk.x+normalk.y*normalk.y+normalk.z*normalk.z); //norm of the normal vector
        float3 normalk_n = make_float3(normalk.x/radius,normalk.y/radius,normalk.z/radius); //normalized normal vector

        //global normalized normal vectors for step k
        normalsW_gpu[pixel].x=T_const6[0]*normalk_n.x + T_const6[1]*normalk_n.y + T_const6[2]*normalk_n.z + T_const6[3];
        normalsW_gpu[pixel].y=T_const6[4]*normalk_n.x + T_const6[5]*normalk_n.y + T_const6[6]*normalk_n.z + T_const6[7];
        normalsW_gpu[pixel].z=T_const6[8]*normalk_n.x + T_const6[9]*normalk_n.y + T_const6[10]*normalk_n.z + T_const6[11];

        if(verticesW_gpu[pixel].y>th_outlier_y){//if poinst are under the floor it sets to zero ***************************
            verticesW_gpu[pixel].x=0;
            verticesW_gpu[pixel].y=0;
            verticesW_gpu[pixel].z=0;
        }
        verticesY_gpu[pixel]=verticesW_gpu[pixel].y;//stores y component of the points
    }//index in v direction (vertical)
}


/**
 * @brief my_kernelFloorND identifies if a point  has a normal vector parallel to the gravity vector and if its y component is greater than a threshold
 * @param verticesW_gpu global vertices in device memory
 * @param normalsW_gpu global normal vectors in device memory
 * @param verFlags_gpu flag that is set to 1 if a point (vertex) has a normal vector parallel to the gravity vector and if its y component is greater than a threshold
 * @param max_val maximum value in y component of all the points
 * @param cols number of columns of the image
 * @param rows number of rows of the image
 * @param th_normal_y threshold that defines if a normalized normal vector is parallel to the gravity vector
 * @param part_max_val part of the maximum value in y coordinate that defines if a point is close to the floor
 */
__global__ void my_kernelFloorND(kernel_pc::Vertex *verticesW_gpu, kernel_pc::Normal *normalsW_gpu, uint *verFlags_gpu, float max_val, int cols, int rows,
                                 float th_normal_y, float part_max_val){

    int u = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)
    int v = blockDim.y * blockIdx.y + threadIdx.y;//index in v direction (vertical)

    if(u < cols && v < rows){//if indices u and v are in the range of the image
        int pixel = u + v*cols;//index for a 1D array
        verFlags_gpu[pixel]=0;
        if((abs(normalsW_gpu[pixel].y)>th_normal_y) && (verticesW_gpu[pixel].y>0.6)){//If normal vector is parallel to the gravity vector and y
            //component of the point is greater than 0.6 (part_max_val is not used)
            verticesW_gpu[pixel].w=1;//fourth component of global vertex is set to 1
            normalsW_gpu[pixel].w=1;//fourth component of global normal vector is set to 1
            verFlags_gpu[pixel]=1;//flag verfFags_gpu is set to 1
        }
    }
}


/**
 * @brief compactData computes the indices of points for an initial segmentation of the floor (points close to the floor)
 * @param compactedDataId_gpu indices of points for an initial segmentation of the floor (points close to the floor), in device momory
 * @param verFlags_gpu flag that is set to 1 if a point (vertex) has a normal vector parallel to the gravity vector and if its y component is greater than a threshold
 * @param verFlagsScan_gpu indice that increases for each new point with flag verFlags_gpu=1
 * @param cols number of columns of the image
 * @param rows number of rows of the image
 */
__global__ void compactData(uint *compactedDataId_gpu, uint *verFlags_gpu, uint *verFlagsScan_gpu, int cols, int rows){

    int u = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)
    int v = blockDim.y * blockIdx.y + threadIdx.y;//index in v direction (vertical)

    if(u < cols && v < rows){//if indices u and v are in the range of the image
        int pixel = u + v*cols;//index for a 1D array
        if (verFlags_gpu[pixel]==1){//if a point (vertex) has a normal vector parallel to the gravity vector and its y component is greater than a threshold
            compactedDataId_gpu[ verFlagsScan_gpu[pixel] ] = pixel; //indices of points for an initial segmentation of the floor (points close to the floor)
        }
    }
}


/**
 * @brief kernel_ransac It sets 1 to diff_lth_gpu if diff is less than the RANSAC threshold (point belongs to a plane)
 * Next, diff_lth_gpu is used to compute the equation that best fits to points from the initial segmentation of the floor
 * @param verticesW_gpu global vertices in device memory
 * @param compactedDataId_gpu indices of points for an initial segmentation of the floor (points close to the floor), in device momory
 * @param randNum_gpu random number for reading a random point from the initial segmentation of the floor
 * @param diff_lth_gpu array that stores if a point is under a threshold for the equation computed in each iteration
 * @param activePoints number of points that have a normal vector parallel to the gravity vector and their y-component is greater than a threshold
 * @param max_iter_ransac maximum number of iterations for carrying out RANSAC
 * @param th_ransac threshold for defining if a point is close to a plane, in [m]
 */
__global__ void kernel_ransac(kernel_pc::Vertex *verticesW_gpu, uint *compactedDataId_gpu, int *randNum_gpu, uint *diff_lth_gpu, uint activePoints, int max_iter_ransac, float th_ransac){

    int u = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)

    if(u < activePoints){//if index u is in the range defined by the number of points that have a normal vector parallel to the gravity vector and their y-component is greater than a threshold
        for(int i=0;i<max_iter_ransac;i++){// for max_iter_ransac iterations do

            kernel_pc::Vertex verR1=verticesW_gpu[compactedDataId_gpu[randNum_gpu[3*i]]];//reads a random point R1 from the initial segmentation of the floor
            kernel_pc::Vertex verR2=verticesW_gpu[compactedDataId_gpu[randNum_gpu[3*i+1]]];//reads a random point R2 from the initial segmentation of the floor
            kernel_pc::Vertex verR3=verticesW_gpu[compactedDataId_gpu[randNum_gpu[3*i+2]]];//reads a random point R3 from the initial segmentation of the floor

            float3 vecV1=make_float3(verR2.x-verR1.x,verR2.y-verR1.y,verR2.z-verR1.z);//computes a vector V1 defined by points R1 and R2
            float3 vecV2=make_float3(verR3.x-verR1.x,verR3.y-verR1.y,verR3.z-verR1.z);//computes a vector V2 defined by points R1 and R3

            float3 normal = cross(vecV1,vecV2);//computes cross product between vectors V1 and V2

            float norm_vn=sqrt(normal.x*normal.x+normal.y*normal.y+normal.z*normal.z);//computes the norm of the normal vector

            float3 normal_vnn=make_float3(normal.x/norm_vn,normal.y/norm_vn,normal.z/norm_vn);//computes the normalized normal vector

            float d_value=-normal_vnn.x*verR1.x-normal_vnn.y*verR1.y-normal_vnn.z*verR1.z;//computes the parameter d of the equation: ax+by+cz+d=0

            kernel_pc::Vertex verP1=verticesW_gpu[compactedDataId_gpu[u]];//reads each point from the initial segmentation of the floor. It is called P1

            float diff=normal_vnn.x*verP1.x+normal_vnn.y*verP1.y+normal_vnn.z*verP1.z+d_value;//verP1 (P1) is evaluated in the equation: ax+by+cz+d. diff is the result of this evaluation

            diff_lth_gpu[i*activePoints+u]=0;
            if(abs(diff)<th_ransac){//if absolute value of diff is under a threshold, then the point is considered to be part of the plane defined by the equation
                diff_lth_gpu[i*activePoints+u]=1;//array that stores if a point is under a threshold for the equation computed in each iteration.
            }
        }
    }
}


__constant__ float transf_const[16]; /**< constant memory for a transformation according to the plane obtained with RANSAC */
__constant__ float T_const7[4]; /**< constant memory for a transformation matrix (for global coordinates) */

/**
 * @brief kernel_Occupancy2DGrid computes two grids: occupied/free, where each cell has the number of proyected points with y-coordinate over/under a threshold
 * @param verticesWnew_gpu vertices after applying the transformation according to the plane obtained with ransac
 * @param OccuF_gpu 2D grid that stores the number of free points (points with y-coordinate over a threshold) proyected over each cell.
 * @param OccuO_gpu 2D grid that stores the number of occupied points (points with y-coordinate under a threshold) proyected over each cell.
 * @param verticesW_gpu global vertices in device memory
 * @param cell_m long of a squared cell, in meters
 * @param size_c_x number of cells in x direction
 * @param size_c_z number of cells in z direction
 * @param cols number of columns of the image
 * @param rows number of rows of the image
 * @param person_xm x coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
 * @param person_zm z coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
 * @param min_dis_user minimum distance over which the occupancy 2D grid is updated, in [m]
 * @param th_occupancy_grid threshold for defining if a grid is free or occupied by evaluating the y component of the points
 */
__global__ void kernel_Occupancy2DGrid(kernel_pc::Vertex *verticesWnew_gpu, uint *OccuF_gpu, uint *OccuO_gpu, kernel_pc::Vertex *verticesW_gpu, float cell_m, 
                                       float size_c_x, float size_c_z, int cols, int rows, float person_xm, float person_zm, float min_dis_user, float th_occupancy_grid){

    int u = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)
    int v = blockDim.y * blockIdx.y + threadIdx.y;//index in v direction (vertical)

    if(u < cols && v < rows) //if indices u and v are in the range of the image
    {
        int pixel = u + v*cols;//index for a 1D array
        float3 vertW=make_float3(verticesW_gpu[pixel].x,verticesW_gpu[pixel].y,verticesW_gpu[pixel].z);

        //transformation according to the plane obtained with ransac
        float3 vertWnew= make_float3(transf_const[0]*vertW.x + transf_const[1]*vertW.y + transf_const[2]*vertW.z + transf_const[3],
                transf_const[4]*vertW.x + transf_const[5]*vertW.y + transf_const[6]*vertW.z + transf_const[7],
                transf_const[8]*vertW.x + transf_const[9]*vertW.y + transf_const[10]*vertW.z + transf_const[11]);

        //transformation to x and z coordinates. The y coordinate is left unchanged
        verticesWnew_gpu[pixel].x=T_const7[0]*vertWnew.x+T_const7[1]*vertWnew.z+person_xm;
        verticesWnew_gpu[pixel].y=vertWnew.y;
        verticesWnew_gpu[pixel].z=T_const7[2]*vertWnew.x+T_const7[3]*vertWnew.z+person_zm;


        //distance from user to global vertex
        float mag_person_cell=sqrt((verticesWnew_gpu[pixel].x-person_xm)*(verticesWnew_gpu[pixel].x-person_xm)+(verticesWnew_gpu[pixel].z-person_zm)*(verticesWnew_gpu[pixel].z-person_zm));

        if(mag_person_cell>min_dis_user && mag_person_cell<3){ //if distance from user to global vertex is greater than a threshold do*********
            uint C_u=(uint)(round(verticesWnew_gpu[pixel].x/cell_m)+round(size_c_x/2));//coordinate in the 2D grid in u direction. The origin is in the upper left corner of the grid
            uint C_v=(uint)(-round(verticesWnew_gpu[pixel].z/cell_m)+round(size_c_z/2));//coordinate in the 2D grid in v direction. The origin is in the upper left corner of the grid

            uint cell_id=(uint)(C_u+C_v*size_c_x);//index for a 1D array
            if(abs(verticesWnew_gpu[pixel].y)< th_occupancy_grid){//if absolute value of y-component of point is less to a threshold then do
                atomicAdd(&OccuF_gpu[cell_id],1);//increments in 1 a counter for free points of a cell
                verticesWnew_gpu[pixel].w=1;
            }else{
                atomicAdd(&OccuO_gpu[cell_id],1);//increments in 1 a counter for occupied points of a cell
                verticesWnew_gpu[pixel].w=0;
            }
        }//end if(mag_person_cell>4)
    }//end if
}//end global



/**
 * @brief kernel_Occupancy computes two grids for identifing free and occupied cells
 * @param OccuFW_gpu 2D grid that stores cells with 1 if a cell is free and 0 otherwise
 * @param OccuOW_gpu 2D grid that stores cells with 1 if a cell is occupied and 0 otherwise
 * @param OccuF_gpu 2D grid that stores the number of free points (points with y-coordinate over a threshold) proyected over each cell.
 * @param OccuO_gpu 2D grid that stores the number of occupied points (points with y-coordinate under a threshold) proyected over each cell.
 * @param size_c_x number of cells in x direction
 * @param size_c_z number of cells in z direction
 * @param downsample_scale scale for downsampling the images
 */
__global__ void kernel_Occupancy(uint *OccuFW_gpu, uint *OccuOW_gpu, uint *OccuF_gpu, uint *OccuO_gpu, float size_c_x, float size_c_z, int downsample_scale){

    int cu = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)
    int cv = blockDim.y * blockIdx.y + threadIdx.y;//index in v direction (vertical)

    if(cu < (int)size_c_x && cv < (int)size_c_z){//if indices cu and cv are in the range of the grid
        int cell = cu + cv*size_c_x;//index for a 1D array
        if (OccuF_gpu[cell]>OccuO_gpu[cell] && OccuF_gpu[cell] > 200){//if number of proyected free points is greater than number of proyected occupied points and
            // than 200 then do
            OccuFW_gpu[cell]=1;//sets cell of OccuFW_gpu to 1
            OccuOW_gpu[cell]=0;//sets cell of OccuOW_gpu to 0
        }else{
            if (OccuO_gpu[cell]>OccuF_gpu[cell] && OccuO_gpu[cell]>20/downsample_scale){//if number of proyected free points is smaller than number of proyected occupied points and the last one is greater than a threshold then do
                OccuOW_gpu[cell]=1;//sets cell of OccuOW_gpu to 1
                OccuFW_gpu[cell]=0;//sets cell of OccuFW_gpu to 0
            }
        }
    }
    int cc=(size_c_x/2)+((size_c_z/2)*size_c_x);
    OccuFW_gpu[cc]=1;//sets cell of OccuFW_gpu to 1
    OccuOW_gpu[cc]=0;//sets cell of OccuOW_gpu to 0
}


/**
 * @brief kernel_reactive_wandering computes an array of six elements with the number of occupied cells inside each cone of the semicircle around the user
 * @param OccuOW_gpu 2D grid that stores cells with 1 if a cell is occupied and 0 otherwise
 * @param size_c_x number of cells in x direction
 * @param size_c_z number of cells in z direction
 * @param cell_m long of a squared cell, in meters
 * @param person_xm coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
 * @param person_zm coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
 * @param person_angle orientation of the user. It is updated by reading the camera pose of the ZED Mini
 * @param th_obst_mag threshold for the distance from an obstacle to the user
 * @param reactnav_gpu array of six elements with the number of occupied cells inside each cone of the semicircle around the user
 */
__global__ void kernel_reactive_wandering(uint *OccuOW_gpu, float size_c_x, float size_c_z, float cell_m, float person_xm, float person_zm, float person_angle, float th_obst_mag, uint *reactnav_gpu){

    int cu = blockDim.x * blockIdx.x + threadIdx.x;//index in u direction (horizontal)
    int cv = blockDim.y * blockIdx.y + threadIdx.y;//index in v direction (vertical)

    if(cu < (int)size_c_x && cv < (int)size_c_z){//if indices cu and cv are in the range of the grid
        int cell = cu + cv*size_c_x;//index for a 1D array
        if(OccuOW_gpu[cell]==1){//if cell is occupied
            float obstacle_xm=(-(size_c_x/2)+cu)*cell_m;//converts the index of cell cu in x-coordinate, in [m]
            float obstacle_zm=((size_c_z/2)-cv)*cell_m;//converts the index of cell cv in y-coordinate, in [m]
            float mag_obstacle=sqrt((obstacle_xm-person_xm)*(obstacle_xm-person_xm)+(obstacle_zm-person_zm)*(obstacle_zm-person_zm));//computes the distance from the user to the cell
            float ang_obstacle=(atan2((obstacle_zm-person_zm),(obstacle_xm-person_xm))*180/3.141592)-person_angle+90;//computes the angle of the obstacle with respect to the user
            if(ang_obstacle>180){ang_obstacle=ang_obstacle-360;}
            if(mag_obstacle<th_obst_mag){//if the distance from the obstacle to the user is smaller than a threshold
                if(ang_obstacle>0 && ang_obstacle<30){//if angle is betweeb 0 and 30
                    atomicAdd(&reactnav_gpu[0],1);//adds 1 to a counter
                }else{
                    if(ang_obstacle>=30 && ang_obstacle<60){//if angle is betweeb 30 and 60
                        atomicAdd(&reactnav_gpu[1],1);//adds 1 to a counter
                    }else{
                        if(ang_obstacle>=60 && ang_obstacle<90){//if angle is betweeb 60 and 90
                            atomicAdd(&reactnav_gpu[2],1);//adds 1 to a counter
                        }else{
                            if(ang_obstacle>=90 && ang_obstacle<120){//if angle is betweeb 90 and 120
                                atomicAdd(&reactnav_gpu[3],1);//adds 1 to a counter
                            }else{
                                if(ang_obstacle>=120 && ang_obstacle<150){//if angle is betweeb 120 and 150
                                    atomicAdd(&reactnav_gpu[4],1);//adds 1 to a counter
                                }else{
                                    if(ang_obstacle>=150 && ang_obstacle<180){//if angle is betweeb 150 and 180
                                        atomicAdd(&reactnav_gpu[5],1);//adds 1 to a counter
                                    }
                                }//end else 120 150
                            }//end else 90 120
                        }//end else 60 90
                    }//end else 30 60
                }//end else 0 30
            }//end if mag_obstacle
        }//end if occuFW
    }//end if cu
}



//sums 1s for defining the parameters with most concordant points (equation of the plane that best fits to data)
void kernel_pc::GetSums(MatrixXf &sumPointsParamsPlane, int activePoints, int max_iter_ransac) 
{
    for(int i=0;i<max_iter_ransac;i++){
        sumPointsParamsPlane(i,0) =(float)(thrust::reduce(thrust::device_ptr<uint>(diff_lth_gpu+activePoints*i),
                                                          thrust::device_ptr<uint>(diff_lth_gpu + activePoints*(i+1))));
    }
}



//compute global vertices, global normal vectors, makes an initial floor segmentation, refines the initial segmentation with RANSAC, builds an occupancy 2D grid and
//carries out reactive navigation. RANSAC is carried out only once, for the first iteration.
void kernel_pc::GetVerticesNorms(Mat Twc, int cols, int rows,int count, float Dperson_xm, float Dperson_zm, float Dperson_angle, float Dth_obst_mag,
                                 int downsample_scale, uint *reactiveNav, bool &isok){


    //parameters for reactive natigation
    person_xm=Dperson_xm;//coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
    person_zm=Dperson_zm;//coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
    person_angle=Dperson_angle;//orientation of the user. It is updated by reading the camera pose of the ZED Mini
    th_obst_mag=Dth_obst_mag;//threshold for the distance from an obstacle to the user

    float *T_host = (float*) Twc.data; //constant memory for a transformation matrix (current transformation of the user)
    cudaMemcpyToSymbol(T_const6, T_host, 3 * 4 * sizeof (float));

    dim3 nThreadsP(NTHREADS_BLOCK, NTHREADS_BLOCK  , 1);//defines the number of threads and blocks (takes advantage of the image structure)
    dim3 nBlocksP(divUpN(cols, nThreadsP.x), divUpN(rows, nThreadsP.y),1);

    //calls the kernel my_kernelVN. It computes global vertices and global normal vectors using the current transformation of the user
    my_kernelVN<<<nBlocksP,nThreadsP>>>(verticesW_gpu,normalsW_gpu, verticesY_gpu, cols, rows, th_outlier_y);
    cudaThreadSynchronize();

    max_val=1;
    //calls the kernel my_kernelFloorND. It identifies if a point  has a normal vector parallel to the gravity vector and if its y component is greater than a threshold
    my_kernelFloorND<<<nBlocksP,nThreadsP>>>(verticesW_gpu,normalsW_gpu, verFlags_gpu, max_val, cols, rows, th_normal_y, part_max_val);
    cudaThreadSynchronize();

    //gets an index for points close to the floor. It uses thrust library
    thrust::exclusive_scan(thrust::device_ptr<unsigned int>(verFlags_gpu),
                           thrust::device_ptr<unsigned int>(verFlags_gpu+cols*rows),
                           thrust::device_ptr<unsigned int>(verFlagsScan_gpu));

    //computes the number of active points (points that have a normal vector parallel to the gravity vector and  y-component greater than a threshold)
    uint lastScanElement;
    cudaMemcpy((void *) &lastScanElement,
               (void *)(verFlagsScan_gpu + cols*rows-1),
               sizeof(uint), cudaMemcpyDeviceToHost);
    activePoints = lastScanElement;//total number of active Points
    //std::cout<<"activePoints: "<< activePoints <<std::endl;

    //calls the kernel compactData. It computes the indices of points for an initial segmentation of the floor (points close to the floor)
    compactData<<<nBlocksP,nThreadsP>>>(compactedDataId_gpu,verFlags_gpu, verFlagsScan_gpu, cols, rows);
    cudaThreadSynchronize();

    //copies data from device to host: global vertices, global normal vectors and indices for points of an initial floor segmentation
    cudaMemcpy(verticesW_host, verticesW_gpu, cols*rows*sizeof (Vertex), cudaMemcpyDeviceToHost);
    cudaMemcpy(normalsW_host, normalsW_gpu, cols*rows*sizeof (Normal), cudaMemcpyDeviceToHost);
    cudaMemcpy(compactedDataId_host, compactedDataId_gpu, cols*rows*sizeof (uint), cudaMemcpyDeviceToHost);
    cudaThreadSynchronize();

    float maxv=0;
    if(activePoints>10000/downsample_scale){// if activePoints is greater than a threshold then

        max_range=activePoints-1;//number of points of the initial floor segmentation

        dim3 nThreadsR(NTHREADS_BLOCK, 1  , 1);//defines the number of threads and blocks
        dim3 nBlocksR(divUpN(activePoints, nThreadsR.x), 1,1);

        for(int j=0;j<3*max_iter_ransac;j++){//computes random indices for performing RANSAC
            randNum_host[j]= rand()%(max_range-min_range + 1) + min_range;
        }

        //copies random indexes from host (cpu) to device (gpu)
        cudaMemcpy(randNum_gpu, randNum_host, 3*max_iter_ransac*sizeof (int), cudaMemcpyHostToDevice);

        //calls kernel kernel_ransac. It sets 1 to diff_lth_gpu if diff is less than the RANSAC threshold (point belongs to a plane)
        kernel_ransac<<<nBlocksR,nThreadsR>>>(verticesW_gpu, compactedDataId_gpu, randNum_gpu, diff_lth_gpu, activePoints, max_iter_ransac, th_ransac);
        cudaThreadSynchronize();

        //sums 1s for defining the parameters with most concordant points (equation of the plane that best fits to data)
        MatrixXf sumPointsParamsPlane(max_iter_ransac,1);
        GetSums(sumPointsParamsPlane, activePoints, max_iter_ransac);

        MatrixXf::Index maxRow, maxCol;
        maxv = sumPointsParamsPlane.maxCoeff(&maxRow, &maxCol);//gets the indices of the points that produced the most concordant parameters of the plane
        //std::cout<<"maxv: "<<maxv <<std::endl;
        if(maxv>90000){//if number of points that fulfill the equation of the plane is greater than a threshold
            get_VN_plane(normal_plane, d_plane, verR1, compactedDataId_host,randNum_host,(int)maxRow); //gets the normal and d parameter of the most concordant plane (ax+by+cz+d=0)

            //gets angles alpha and beta for moving the plane of the floor to the plane XZ (y coordinate equal to zero)
            float diag_yz=sqrt(normal_plane.y*normal_plane.y+normal_plane.z*normal_plane.z);
            alpha=abs(acos(diag_yz));
            beta=abs(acos(abs(normal_plane.y)/diag_yz));
            if(normal_plane.y<0 && normal_plane.x>0){
                alpha=-1*alpha;
            }else{
                if(normal_plane.y>0 && normal_plane.x<0){
                    alpha=-1*alpha;
                }
            }

            if(normal_plane.y<0 && normal_plane.z<0){
                beta=-1*beta;
            }else{
                if(normal_plane.y>0 && normal_plane.z>0){
                    beta=-1*beta;
                }
            }

        }//end maxv
    }//end activePoints

    Matrix4f transf,transf_t;
    get_transform(transf, alpha,beta, verR1,d_plane, normal_plane);//gets transformation matrix using alpha and beta
    transf_t=transf.transpose();

    float *transf_host = (float*) transf_t.data();//constant memory for a transformation according to the plane obtained with RANSAC
    cudaMemcpyToSymbol(transf_const, transf_host, 4 * 4 * sizeof (float));

    cudaMemset(OccuF_gpu, 0X00, (int) size_c_x * size_c_z*sizeof(uint)); //sets to zero OccuF_gpu
    cudaMemset(OccuO_gpu, 0X00, (int) size_c_x * size_c_z*sizeof(uint)); //sets to zero OccuO_gpu

    float ang=(-person_angle+90);
    if(ang>180){ang=ang-360;}
    float ang_rad=ang*3.141592/180;

    Mat Tperson=(cv::Mat_<float>(2,2) <<       cos (ang_rad),		sin (ang_rad),
                 -sin (ang_rad),		cos (ang_rad));

    float *T_host_person = (float*) Tperson.data; //constant memory for a transformation matrix (for global coordinates)
    cudaMemcpyToSymbol(T_const7, T_host_person, 2 * 2 * sizeof (float));

    //calls the kernel kernel_Occupancy2DGrid. It computes two grids: occupied/free, where each cell has the number of proyected points with y-coordinate over/under a threshold
    kernel_Occupancy2DGrid<<<nBlocksP,nThreadsP>>>(verticesWnew_gpu, OccuF_gpu, OccuO_gpu, verticesW_gpu, cell_m, size_c_x, size_c_z, cols, rows, person_xm, person_zm, min_dis_user, th_occupancy_grid);
    cudaThreadSynchronize();

    //copies data from device to host: new global vertices, grids that store the number of free and occupied points proyected over each cell
    cudaMemcpy(verticesWnew_host, verticesWnew_gpu, cols*rows*sizeof (Vertex), cudaMemcpyDeviceToHost);

    dim3 nThreadsO(NTHREADS_BLOCK, NTHREADS_BLOCK  , 1);//defines the number of threads and blocks for working with the grid
    dim3 nBlocksO(divUpN((int)size_c_x, nThreadsO.x), divUpN((int)size_c_z, nThreadsO.y),1);

    if(count==1){//If it is the first iteration do (keeps previous information stored in the grids for k>1)
        cudaMemset(OccuFW_gpu, 0X00, (int) size_c_x * size_c_z*sizeof(uint)); //sets to zero OccuFW_gpu
        cudaMemset(OccuOW_gpu, 0X00, (int) size_c_x * size_c_z*sizeof(uint)); //sets to zero OccuOW_gpu
    }

    //calls kernel kernel_Occupancy. It computes two grids for identifing free and occupied cells
    kernel_Occupancy<<<nBlocksO,nThreadsO>>>(OccuFW_gpu, OccuOW_gpu, OccuF_gpu, OccuO_gpu, size_c_x, size_c_z, downsample_scale);
    cudaThreadSynchronize();

    //copies data from device to host: grids that identify free and occupied cells
    cudaMemcpy(OccuFW_host, OccuFW_gpu, (int) size_c_x * size_c_z*sizeof(uint), cudaMemcpyDeviceToHost);
    cudaMemcpy(OccuOW_host, OccuOW_gpu, (int) size_c_x * size_c_z*sizeof(uint), cudaMemcpyDeviceToHost);

    cudaMemset(reactnav_gpu, 0X00, 6*sizeof(uint)); //sets to zero reactnav_gpu

    dim3 nThreadsO1(NTHREADS_BLOCK, NTHREADS_BLOCK  , 1);//defines the number of threads and blocks for working with the grid
    dim3 nBlocksO1(divUpN((int)size_c_x, nThreadsO1.x), divUpN((int)size_c_z, nThreadsO1.y),1);

    //calls the kernel kernel_reactive_wandering. It computes an array of six elements with the number of occupied cells inside each cone of the semicircle around the user
    kernel_reactive_wandering<<<nBlocksO1,nThreadsO1>>>(OccuOW_gpu, size_c_x, size_c_z, cell_m, person_xm, person_zm, person_angle, th_obst_mag, reactnav_gpu);
    cudaThreadSynchronize();

    //copies data from device to host: array of six elements with the number of occupied cells inside each cone of the semicircle around the user
    cudaMemcpy(reactnav_host, reactnav_gpu, 6*sizeof(uint), cudaMemcpyDeviceToHost);

    for(int k=0;k<6;k++){//copies data to the array reactiveNav
        reactiveNav[k]=reactnav_host[k];
    }
}


//frees host and device memory and unbinds texture memory
void kernel_pc::Unbind(){

    //unbinds texture memory
    cudaUnbindTexture(texVertK);
    cudaUnbindTexture(texDepthRC);

    //frees device memory
    cudaFree(depth_gpu);

    cudaFree(verticesK_gpu);
    cudaFree(verticesW_gpu);
    cudaFree(normalsW_gpu);

    cudaFree(verticesY_gpu);
    cudaFree(verFlags_gpu);
    cudaFree(verFlagsScan_gpu);
    cudaFree(compactedDataId_gpu);

    cudaFree(randNum_gpu);
    cudaFree(diff_lth_gpu);
    cudaFree(verticesWnew_gpu);
    cudaFree(OccuF_gpu);
    cudaFree(OccuO_gpu);
    cudaFree(OccuFW_gpu);
    cudaFree(OccuOW_gpu);

    cudaFree(reactnav_gpu);

    //frees host memry
    free(verticesW_host);
    free(normalsW_host);
    free(compactedDataId_host);
    free(randNum_host);
    free(OccuF_host);
    free(OccuO_host);
    free(verticesWnew_host);
    free(OccuFW_host);
    free(OccuOW_host);
    free(reactnav_host);
}


//gets the normal and d parameter of the most concordant plane (ax+by+cz+d=0)
void kernel_pc::get_VN_plane(kernel_pc::Normal &normal_plane, float &d_plane, kernel_pc::Vertex &verR1, uint *compactedDataId_host, int *randNum_host, int maxRow){

    kernel_pc::Vertex vecV1,vecV2;//vector V1 and V2
    kernel_pc::Normal normal;//normal vector
    float norm_vn;

    int randNum1=randNum_host[maxRow*3];
    verR1=verticesW_host[compactedDataId_host[randNum1]];//random point R1 from the initial segmentation of the floor that generated the most concordant plane

    int randNum2=randNum_host[maxRow*3+1];
    kernel_pc::Vertex verR2=verticesW_host[compactedDataId_host[randNum2]]; //random point R2 from the initial segmentation of the floor that generated the most concordant plane
    int randNum3=randNum_host[maxRow*3+2];
    kernel_pc::Vertex verR3=verticesW_host[compactedDataId_host[randNum3]];//random point R3 from the initial segmentation of the floor that generated the most concordant plane
    vecV1.x=verR2.x-verR1.x;
    vecV1.y=verR2.y-verR1.y;
    vecV1.z=verR2.z-verR1.z;//vector V1 defined by points R1 and R2
    vecV2.x=verR3.x-verR1.x;
    vecV2.y=verR3.y-verR1.y;
    vecV2.z=verR3.z-verR1.z;//vector V2 defined by points R1 and R3
    normal.x=vecV1.y*vecV2.z-vecV2.y*vecV1.z;
    normal.y=vecV2.x*vecV1.z-vecV1.x*vecV2.z;
    normal.z=vecV1.x*vecV2.y-vecV2.x*vecV1.y;//computes the normal vector
    norm_vn=sqrt(normal.x*normal.x+normal.y*normal.y+normal.z*normal.z);
    normal_plane.x=normal.x/norm_vn;
    normal_plane.y=normal.y/norm_vn;
    normal_plane.z=normal.z/norm_vn;//computes the normalized normal vector
    d_plane=-normal_plane.x*verR1.x-normal_plane.y*verR1.y-normal_plane.z*verR1.z;//computes the parameter d of the equation: ax+by+cz+d=0
}	

//gets transformation matrix using alpha and beta. It moves the plane of the floor to the plane XZ (y coordinate equal to zero)
void kernel_pc::get_transform(Matrix4f &transf, float alpha, float beta, kernel_pc::Vertex verR1, float d_plane, kernel_pc::Normal normal_plane){



    Matrix4f rotx, rotz,trasl,trasl2;
    rotx << 1,        0,          0,              0,
            0,        cos(beta), -sin(beta),    0,
            0,        sin(beta), cos(beta),     0,
            0,        0,          0,              1;//rotation of beta around x axis

    rotz << cos(alpha), -sin(alpha),   0,              0,
            sin(alpha),  cos(alpha),   0,              0,
            0,            0,           1,              0,
            0,            0,           0,              1;//rotation of alpha around z axis

    trasl << 1,      0,      0,      normal_plane.x*d_plane,
            0,      1,      0,      normal_plane.y*d_plane,
            0,      0,      1,      normal_plane.z*d_plane,
            0,      0,      0,      1;//translation of [0 -abs(d_plane) 0 1]

    trasl2<< 1,      0,      0,      +verR1.x,
            0,      1,      0,          0,
            0,      0,      1,      +verR1.z,
            0,      0,      0,      1;

    //transf=trasl2*rotx*rotz*trasl;
    transf=rotx*rotz*trasl;//composed transformation

}

