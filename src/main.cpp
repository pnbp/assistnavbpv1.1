//============================================================================
// Name        : main.cpp
// Authors     : Andres Diaz - Sixto Campaña - Eduardo Caicedo
// Version     : 1.1
// Copyright   : All rights reserved
// Description : AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1
//============================================================================

/**
 * @mainpage AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1
 *
 * \section Introduction
 *
 Wandering environments is a fundamental task for developing every day activities. However, blind people have great difficulty for moving,
 especially in unknown environments, which reduces their autonomy and puts them at risk of suffering an accident. We took techniques from
 mobile robotics, autonomous cars and computer vision, and together with a high-performance and wearable computer and a stereo camera, we
 implemented a system that assists blind people to wander unknown environments in a safe way, by sensing the world, segmenting the floor,
 creating an occupancy 2D grid, reacting to close obstacles and generating vibration patterns with an haptic belt.
 *
 *
 * Considering the big amount of data that a stereo camera delivers, the fact that the person is moving in a highly dynamic
environment and the need of providing immediate feedback to the user, we defined two constraints: 1. Depth and color data
must be processed at real time, in a general purpose graphics processing unit GPGPU. 2. Images must be processed in a
wearable computer, it means, in a light and small processing device. Moreover, we defined trade-offs between sensing,
computation and system usability that allow the system fulfill these requirements appropriately.

The system shows an OpenGL window with the occupancy 2D grid. Besides the
OpenGL window, two OpenCV windows are shown: one for the color image and the other one for the depth image. This data, together with the positional tracking, is processed at real time and comes either from a stored SVO file or from the
stereo Camera ZED Mini. This source of data is selected by the user before executing the program.

In the upper left corner of the OpenGL window, information about the activation of the motors of the haptic belt is shown. If occupied cells are inside the semicircle around the user that is wearing the
system, the motors are activated (white point represents a deactivated motor while red point represents an activated motor). If occupied cells are in the region between 120 and 180 degrees, the
left motor is activated. If occupied cells are in the region between 60 and 120 degrees,
the middle motor is activated. If occupied cells are in the region between 0 and 60 degrees, the right motor is activated. The connection, using serial communication, with a real haptic belt is optional
and the system will run with or without this belt.

 For avoiding wrong estimations of the plane, RANSAC is applied only if the number of points resulting from the initial segmentation (activePoints) is greater than a threshold (th_aPoints=90000). A low value of activePoints
happens when the user is close to an obstacle and the floor is not visible. In these cases, the equation of the plane that was computed with enought activePoints in the closest pose
 to the current one, is used for applying the transformations. This is the main difference of version 1.0 with respect to version 1.1.



 * \section Installation
 *
The Jetson TX2 has the dual-core Nvidia Denver2 + quad-core ARM Cortex-A57, a graphics processing unit for speeding up the data processing: the Nvidia Pascal with 256 cores and
 8GB of memory, and Ubuntu 18.04 as operting system. JetPack SDK 4.3 is used to flash the Jetson TX2 developer kit with the operating system image, and to install developer tools and libraries. It installs
OpenCV 4.1 and Cuda 10. The library of the ZED Camera, the ZED SDK 3.2.2 for JetPack 4.3, is used for capturing depth data, color data, and positional tracking. OpenCV
is employed for visualizing depth and color images and for carrying out basic
 operations with images. OpenGL (freeGLUT) allows the system to draw the occupancy 2D grid. Most of data processing is carried out
 on GPU, using Cuda 10, achieving to speed up the performance and to process data at real time. The activation of the motors of an haptic belt are represented
 as points (in white if a motor is deactivated and in red if a motor is activated) on the upper left corner of the window of OpenGL.
The system can optionally communicate with the control box of an haptic belt (version 1 and 2 of the belt) that has
vibrating motors, using the library <a href="https://www.teuniz.net/RS-232/">RS-232 </a>, in order to send motion commands to the user. To use this library you need to put the rs232.c file in the src folder and the rs232.h file in include folder.
 *
 You can build and execute the project with Qtcreator by following the next steps (recommended in host computer).
 *
 * 1. Save the project "AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1".
 * 2. Install the libraries: ZED, OpenCV, OpenGL, Cuda, Thurst, Gflags, Glogs. Follow the instructions given by each
 * library. Remember that OpenCV, Cuda and Thrust are installed with JetPack, in both host computer and developer kit.
 * 3. Open QtCreator. Click on "Open project" in the "Welcome" screen. Browse the project and select the CMakeLists.txt file located
 * in the main folder (first level).
 * 4. Choose a location for the folder where the project will be built.
 * 5. Run the CMake. If the libraries are found, Click on "Finish".
 * 6. The configuration of the project and the modules required for its normal performance is carried out automatically, using
 * the CMakeLists file, located in the main folder of the project (first level), and the FindGFlags and FindGLog files, located in the cmake folder. If some library is not found, manual configuration must be done.
 *
 *
Optionally, you can build the project without Qtcreator (recommended in Jetson TX2) by creating a build folder, running cmake and make, as follows.
- cd <folder of the project>
- sudo mkdir build
- cd build
- sudo cmake ..
- sudo make

Next, in the build forder, you can execute the application by typing: sudo ./AssistNavBP

 *
 * \section Configuration
 *
 * The main file is src/main.cpp. It defines the order of execution of the functions. It makes instances of the class kernel_pc and
  calls the most important functions for initializing vairables, allocating host and device memory, loading/capturing depth, RGB images and positional tracking,
computing local vertices, computing global vertices and normal vectors, making an initial floor segmentation, refining the initial segmentation with RANSAC, building an occupancy 2D grid and
carrying out reactive navigation. RANSAC is carried out for each frame if the number of activePoints is over a threshold. Moreover, it manages the functions and events for visualizing the occupancy 2D grid,
 the images captured with the camera and the communication with the haptic belt (if it is connected).
 *
 * The general setting of the project can be done using Gflags through command line (before executing the program).
 * The parameters are listed next.
  - g_load_svo (true by default), a boolean variable. If true then a SVO file defined by the string svo_input_path is loaded. Otherwise, the Camera ZED Mini is used
  - g_svo_input_path ("/usr/local/zed/sample/svoFiles/prueba1.svo" by default), a string that defines the address and name of a svo file.
  - g_size_m_x (20 by default), a double variable that defines the size of the grid in x direction, in [m]
  - g_size_m_z (20 by default), a double variable that defines the size of the grid in z direction (y axis is parallel to gravity vector), in [m]
  - g_cell_m (0.1 by default), a doble variable for setting the long of a squared cell that belongs to the grid, in [m]
  - g_th_obst_mag (0.8 by default), a double variable for setting the threshold in distance that defines if an obstacle is close to the user, generating risk of collision, in [m]
  - g_max_iter_ransac (104 by default), an integer variable that defines the maximum  number of iterations for RANSAC
  - g_th_ransac (0.06 by default), a double variable for setting the threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]
  - g_th_occupancy_grid (0.2 by default), a double variable that defines the threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m].
  - g_downsample_scale (1 by default), an integer variable that defines the scale for downsampling the images
*
*
* You can copy the following text in command line and modify it according to the requirements.
*
*


 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  sudo /<adress of the build folder of the project>/AssistNavBP
   --g_load_svo true --g_svo_input_path /<adress of the folder where the svo files are located>/svofile.svo
   --g_size_m_x 20 --g_size_m_z 20 --g_cell_m 0.1 --g_th_obst_mag 0.8
   --g_max_iter_ransac 104 --g_th_ransac 0.06 --g_th_occupancy_grid 0.2 --g_downsample_scale 1
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *
* You can see the description of the flags by typing in command line: sudo ./AssistNavBP --helpon main. Moreover, you can set only some flags. The remaining flags will be set automatically
* to the default value defined in the main.cpp file.
*
* Other option with Gflags is to edit and execute (sudo sh AssistNavBP.launch) a lauch file,
* located in the launch folder and that contains the previous parameters.
 *

 There are other events that work when the OpenCV windows, used for showing the color and depth images, are activated. The "e" key finishes the while loop and the "q" key finishes the loop
 for visualizing the OpenGL window and the main program finishes. It is an alternative to "ESC" key to finish the main program but it works when any of the OpenCV window is activated

 *
 *
 *
 Other parameters can be modified directly from the code (before executing the program):
 *
 - Parameters in main.cpp for setting the serial communication, using the RS-232 library: port (c_port_nr, 16 for ttyS0 by default), baudrate (bdrate, 9600 by default),
 communication mode (mode, '8','N','1',0 by default).
 - Parameters in main.cpp for setting the stereo camera ZED Mini: resolution (camera_resolution, sl::RESOLUTION_VGA (height 376,width 672) by default), frames per second (camera_fps, 15 fps by default),
  cordinate system (coordinate_system, sl::COORDINATE_SYSTEM_IMAGE by default), depth mode (depth_mode, sl::DEPTH_MODE_QUALITY by default),
  units of the coordinates (coordinate_units, sl::UNIT_METER by defaut), minimum distance in depth (depth_minimum_distance, 0.8 by default), maximum distance in depth
 (setDepthMaxRangeValue, 4 by default), sensing mode (sensing_mode, sl::SENSING_MODE_STANDARD by default).
 - Parameters in kernel_pc.cu for avoiding outliers and carrying out the initial segmentation of the point cloud: threshold that defines if a point is an outlier by evaluating its y coordinate, in m
  (th_outlier_y, 3.0 by default), threshold that defines if a normalized normal vector is parallel to the gravity vector (th_normal_y, 0.8 by default), threshold in y coordinate that
 defines if a point is close to the floor (0.8 by default), minimum distance over which the occupancy 2D grid is updated, in m (min_dis_user, 0.4 by default).
 *
 *
 *
 * \section started Getting Started
 *
 * The application can be executed using either Qtcreator or the terminal (see Configuration section). If the parameter load_svo=true then the file
 * located in the path svo_input_path will be loaded and the images will be processed for computing the occupancy 2D grid. The svo file contains in compressed way, depth, color and positional data, captured
 * previously. The grid is built incrementaly as the user moves and covers more space. When the application is executed with the parameter
 * load_svo=false then the camera ZED Mini must be connected to the laptop/Jetson TX2 in order to begin to capture data and to process it at real time. The camera must point to the floor and be located on the
 * chest of the user. In both cases, either with svo file or with connected camera, three windows will appear on screen: an OpenGL window
 * for showing the occupancy 2D grid, an OpenCV window for visualizing the color image, and an OpenCV window for visualizing the depth image. The activation of the
 * motors will be visualized on the upper left corner of the OpenGL window. If an haptic belt is used (optional), then the motors will vibrate according to the reactive navigation, executed by the system.
 *
 * The authors are not responsible in any manner for any injure or accident which may occur by testing the system in real environments and with people carrying out tasks that
 * involve motion.
 *
 * \section Authors
 *
 *  - Andres Diaz, Ph.D. Universidad Nacional Abierta y a Distancia UNAD
 *  - Sixto Campaña, Ph.D. Universidad Nacional Abierta y a Distancia UNAD
 *  -  Eduardo Caicedo, Ph.D. Universidad del Valle
 * $Date: 2020/05/05 17:36:00 $
 *
 * \version 1.1
 *
 * Contact: andres.diaz@unad.edu.co
 *
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <fstream>
#include <math.h>

#include <eigen3/Eigen/Dense>

#include <gflags/gflags.h>
#include <glog/logging.h>


#include <GL/gl.h>
#include <GL/glu.h>
//#include <GL/glut.h>
#include <GL/freeglut.h>
#include <unistd.h>

#include <sys/time.h>
#include <time.h>

// ZED includes
#include <sl/Camera.hpp>

// OpenCV includes
#include <opencv2/opencv.hpp>

#include <kernel_pc.h>

#include "rs232.h"

/////////////////////////////////////



/**
 * @brief draw_vehicle draws the camera as a mobile coordinate system in OpenGL
 */
void draw_vehicle(void);
/**
 * @brief init_glut initializes the OpenGL parameters
 * @param argc Not used
 * @param argv Not used
 */
void init_glut(int argc, char** argv);
/**
 * @brief keyboard manages the keyboard events. w -> + elevation, s -> - elevation, a -> +azimuth, d -> -azimuth,
 * i -> + distance, k -> - distance, v -> switches the visualization of the image plane, b -> switches the visualization of the
 * volumetric grid, c -> switches the view mode between far and close view mode, and ESC for finishing the visualization of the
 * reconstructed model.
 * @param key key activated
 * @param x Not used
 * @param y Not used
 */
void keyboard(unsigned char key, int x, int y);
/**
 * @brief mouseFunc manages the mouse events for zooming
 * @param button Button activated
 * @param state Type of action that activates the event
 * @param x x coordinate where the the left mouse button was pressed
 * @param y y coordinate where the the left mouse button was pressed
 */
void mouseFunc(int button, int state, int x, int y);
/**
 * @brief idleFunc is the IdleFunc of OpenGL
 */
void idleFunc(void);
/**
 * @brief reshape reshapes the window (fixes the window's size) and its content for OpenGL
 * @param width Current width of the window
 * @param height Current height of the window
 */
void reshape(int width, int height);
/**
 * @brief display displays the local point cloud and the camera pose, according to the view mode
 */
void display(void);
/**
 * @brief displayOccu displays the occupancy 2D grid
 */
void displayOccu(void);
/**
 * @brief draw_Occu_grid2 draws a global occupancy 2d grid by merging local depth data
 * @param size_c_x number of cells in x direction
 * @param size_c_z number of cells in z direction
 * @param cell_m long of a squared cell, in [m]
 */
void draw_Occu_grid2(float size_c_x, float size_c_z, float cell_m);
/**
 * @brief draw_points draws a point cloud computed from local depth data
 */
void draw_points();
/**
 * @brief initLights manages the lights of the scene in OpenGL
 */
void initLights();
/**
 * @brief renderBitmapString displays text on the 3D scene in OpenGL
 * @param x x coordinate that defines the place where to insert text
 * @param y y coordinate that defines the place where to insert text
 * @param z z coordinate that defines the place where to insert text
 * @param font defines the font
 * @param string is the text to be inserted in the 3D scene
 */
void renderBitmapString(float x,float y,float z,void *font,	char *string);
/**
 * @brief draw_trajectory draws the camera trajectory (trajectory of the user)
 */
void draw_trajectory();
/**
 * @brief DrawCircle draws a circle in cx,cz, with radius r. This circle defines posible collisions
 * @param cx x coordinate of center of the circle
 * @param cz z coordinate of center of the circle (y axis is parallel to gravity vector)
 * @param r raious of circle
 * @param num_segments number of segments that makes up the circle
 * @param person_angle orientation of the camera (orientation of the user)
 */
void DrawCircle(float cx, float cz, float r, int num_segments, float person_angle);
/**
 * @brief draw_reactiveNav_func function for reactive navigation. It defines a semicircle with six cones of 30 degrees.
 *  Occupied  cells  inside  the  semicircle  are  considered dangerous. Sections  with  occupied  cells
 * indicate that there is an obstacle in that direction  and  the  user  can  not  traverse  it
 * @param reactiveNav array with six elements with number of occupied cells in each cone of the semicircle
 */
void draw_reactiveNav_func(uint *reactiveNav);
/**
 * @brief belt_func controls the activation of the vibrating motors of the belt
 * @param reactiveNav array with six elements with number of occupied cells in each cone of the semicircle
 * @param cport_nr defines the port. cport_nr=16 is for /dev/ttyS0 on linux and COM1 on windows
 * @param str string that the control box of the belt uses for activating certain motor
 */
void belt_func(uint *reactiveNav, int cport_nr, char str[][512]);

/**
 * @brief ConvertQuat2Mat converts quaternion to matrix
 * @param orqx first component of quaternion
 * @param orqy second component of quaternion
 * @param orqz third component of quaternion
 * @param orqw fourth component of quaternion
 * @return matrix equivalent to quaternion
 */
cv::Mat ConvertQuat2Mat(float orqx, float orqy, float orqz, float orqw);
/**
 * @brief slMat2cvMat Converts sl::Mat to cv::Mat
 * @param input Matrix sl:Mat that will be converted to cv:Mat
 * @return Matrix cv:Mat
 */
cv::Mat slMat2cvMat(sl::Mat& input);
/**
 * @brief GetRPY converts transformation matrix to rpy format, version 1
 * @param cameraRot transformation matrix
 * @return orientation in roll, pitch, yaw format
 */
cv::Mat GetRPY(cv::Mat cameraRot);
/**
 * @brief GetRPY2 converts transformation matrix to rpy format, version 2
 * @param cameraRot transformation matrix
 * @return orientation in roll, pitch, yaw format
 */
cv::Mat GetRPY2(cv::Mat cameraRot);


cv::Mat ConvertEuler2Mat(sl::float3 rote);

void start_end_belt_func(int cport_nr, char str[][512]);

std::string datetime();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief DEFINE_bool uses a boolean variable (g_load_svo), a value by default (true) and a description. It uses gflags for commandline flags processing.
  * g_load_svo is a boolean variable. If true then a SVO file defined by the string svo_input_path is loaded. Otherwise, the Camera ZED Mini is used
 */
DEFINE_bool(g_load_svo, false, "boolean variable. If true then a SVO file defined by the string svo_input_path is loaded. Otherwise, the Camera ZED Mini is used");


/**
  * @brief DEFINE_string uses a string variable (g_svo_input_path), a value by default and a description. It uses gflags for commandline flags processing.
  * g_svo_input_path is the directory and name of the svo file
 */
DEFINE_string(g_svo_input_path, "/home/andresdiaz/assistnavbp2/svo files/p8.svo", "address and name of svo file");


/**
 * @brief DEFINE_double uses a float variable (g_size_m_x), a value by default (20) and a description. It uses gflags for commandline flags processing.
  * g_size_m_x is the size of the grid in x direction, in [m]
 */
DEFINE_double(g_size_m_x, 20, "size of the grid in x direction, in [m]");


/**
 * @brief DEFINE_double uses a float variable (g_size_m_z), a value by default (20) and a description. It uses gflags for commandline flags processing.
  * g_size_m_z is the size of the grid in z direction (y axis is parallel to gravity vector), in [m]
 */
DEFINE_double(g_size_m_z, 20, "size of the grid in z direction (y axis is parallel to gravity vector), in [m]");


/**
 * @brief DEFINE_double uses a float variable (g_cell_m), a value by default (0.05) and a description. It uses gflags for commandline flags processing.
  * g_cell_m is the long of a squared cell that belongs to the grid, in [m]
 */
DEFINE_double(g_cell_m, 0.1, "long of a squared cell that belongs to the grid, in [m]");


/**
 * @brief DEFINE_double uses a float variable (g_th_obst_mag), a value by default (1.5) and a description. It uses gflags for commandline flags processing.
  * g_th_obst_mag is the threshold in distance that defines if an obstacle is close to the user, generating risk of collision, in [m]
 */
DEFINE_double(g_th_obst_mag, 0.8, "threshold in distance that defines if an obstacle is close to the user, generating risk of collision, in [m]");


/**
 * @brief DEFINE_int32 uses an integer variable (g_max_iter_ransac), a value by default (104) and a description. It uses gflags for commandline flags processing.
  * g_max_iter_ransac is the maximum  number of iterations for RANSAC
 */
DEFINE_int32(g_max_iter_ransac, 104, "maximum  number of iterations for RANSAC");

/**
 * @brief DEFINE_double uses a float variable (g_th_ransac), a value by default (0.1) and a description. It uses gflags for commandline flags processing.
  * g_th_ransac is the threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]
 */
DEFINE_double(g_th_ransac, 0.06, "threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]");

/**
 * @brief DEFINE_double uses a float variable (g_th_occupancy_grid), a value by default (0.4) and a description. It uses gflags for commandline flags processing.
  * g_th_occupancy_grid is the threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m]
 */
DEFINE_double(g_th_occupancy_grid, 0.2, "threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m]");


/**
 * @brief DEFINE_int32 uses an integer variable (g_downsample_scale), a value by default (1) and a description. It uses gflags for commandline flags processing.
  * g_downsample_scale is the scale for downsampling the images
 */
DEFINE_int32(g_downsample_scale, 1, "scale for downsampling the images");


////////////////////////////////////////////////////////global variables/////////////////////////////////////////////////////////////////////////////////////////

cv::Mat transf_current_pc(4,4,CV_32FC1), /**< current transformation that defines the current pose of the camera */
transf_current_vis(4,4,CV_32FC1),
transf_inib(4,4,CV_32FC1), /**< rotation around x axis of -beta. It moves the optical axis to a plane parallel to the horizon */
transf_inib_vis(4,4,CV_32FC1),
transf_inib1(4,4,CV_32FC1), transf_inib3(4,4,CV_32FC1);
cv::Mat Transf_accum; ///<accumulated transformations from all camera poses
cv::Mat rpy_current_vis(3,1,CV_32FC1);///<roll, pitch and yaw for the current camera pose
cv::Mat transf_aux;///<auxiliar transformation for storing accumulated transformations

float *colors; ///<color of points for drawing in OpengL. It can be associated to RGB data or to normal vectors data
int n_vert;///<number of local points estimated from local depth data
float *vertices;///<points of the point cloud. It has three components for each point.

float ini_ele = 10 * 3.1415 / 180, /**< initial elevation of the virtual camera in OpenGL */
ini_az = 8.0 * 3.1415 / 180, /**< initial azimuth of the virtual camera in OpenGL */
ini_dis = 15; /**< initial distance of the virtual camera in OpenGL */ //10.5
float az = 0, /**< azimuth of the virtual camera in OpenGL */
ele = 0, /**< elevation of the virtual camera in OpenGL */
dis=0; /**< distance of the virtual camera in OpenGL */
float fov=60,/**< vertical field of view of camera ZED Mini */
focal = 0.03;/**< Focal length of camera ZED Mini in [m] */
float znear=focal-0.0001, /**< Near depth for the frustrum in OpenGL, in [m] */
zfar=1000; /**< Far depth for the frustrum in OpenGL, in [m] */

int new_width; /**< width of the image */
int new_height;  /**< height of the image */
int downsample_scale; /**< scale for downsampling the images */

float txc, /**< x coordinate of the current position of the virtual camera in OpenGL */
tyc, /**< y coordinate of the current position of the virtual camera in OpenGL */
tzc; /**< z coordinate of the current position of the virtual camera in OpenGL */

float size_m_x; /**< size of the grid in x direction, in [m]  */
float size_m_z; /**< size of the grid in z direction (y axis is parallel to gravity vector), in [m] */
float cell_m; /**< long of a squared cell that belongs to the grid, in [m] */

float person_xm,/**< x coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini */
person_zm,/**< z coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini */
person_angle,/**< orientation of the user. It is updated by reading the camera pose of the ZED Mini */
th_obst_mag;/**< threshold in distance that defines if an obstacle is close to the user, generating risk of collision */
uint reactiveNav[6];/**< array of six elements with the number of occupied cells inside each cone of the semicircle around the user */

float th_ransac; /**< threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m] */
int max_iter_ransac;  /**< maximum  number of iterations for RANSAC */

float th_occupancy_grid; /**< threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m] */

kernel_pc::Normal nor;//nor can receibe normal data or rgb data, according to flag flag_color
//Creates my_kernel_pc
kernel_pc my_kernel_pc;

//displays help in console
void printHelp();

//id for texture
GLuint cameraImageTextureID;

///////////////////////////////////////////////variables for setting the system///////////////////////////////

bool shown=true;/**< If it is changed to false, the while loop ends. If it is changed again it ends the loop for visualizing the OpenGL window. It is switched with "ESC" key */
bool draw_grid = true;/**< If true then the occupancy 2D grid is shown. Otherwise, the local point cloud is shown. It is switched with "v" key */
bool flag_color=false; /**< If true then the point cloud is drawn with color associated to RGB data. Otherwise, the points that belong to the floor
 are drawn in green and the remaining points are drawn in white. It switches with "n" key */
bool flag_activePoints=true;/**< If false then the point cloud is drawn with all the local points. Otherwise, only points that belong to the floor are drawn. It switches with "f" key */


///////////////////////////////////////////////////////main/////////////////////////////////////////////////////

/**
 * @brief main Main fuction of the project. It defines the order of execution of the functions. It makes instances of the class kernel_pc and
  calls the most important functions for initializing vairables, allocating host and device memory, loading/capturing depth, RGB images and positional tracking,
computing local vertices, computing global vertices and normal vectors, making an initial floor segmentation, refining the initial segmentation with RANSAC, building an occupancy 2D grid and
carrying out reactive navigation. RANSAC is carried out only once, for the first iteration. Moreover, it manages the functions and events for visualizing the occupancy 2D grid or the
local point cloud, the images captured with the camera and the communication with the haptic belt (if it is connected).
 * @param argc For commandline flags
 * @param argv For commandline flags
 * @return Returns 0 at the end of the program
 */
int main(int argc, char** argv) {//***********************************************************************************************************************************************

    struct timeval tval_before, tval_after, tval_result;
    struct timeval tval_before1, tval_after1, tval_result1;


    // sets the glog library
    google::InitGoogleLogging(argv[0]);
    // sets gflags library
    google::SetUsageMessage( "AssistNavBP, A VISION-BASED A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1" );
    // sets the program version - ideally this comes automagically.
    google::SetVersionString( "1.0" );
    // parses the command line flags
    google::ParseCommandLineFlags( &argc, &argv, true );

    //welcome message that is printed in console
    std::cout<<"Starting AssistNavBP, A VISION-BASED A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1"<<std::endl;

    ///////////////////////////////////////////////setting the file SVO to be loaded if flag load_svo is true//////////////////////////////////////////

    //load SVO files like mysvon1, prueba1
    string svo_input_path=FLAGS_g_svo_input_path;
    //bool load_svo=FLAGS_g_load_svo;/**< If true then a SVO file defined by the string svo_input_path is loaded. Otherwise, the Camera ZED Mini is used. */
    bool load_svo=true;
    std::cout<<"load_svo: "<<load_svo<<std::endl;

    //////////////////////////////////////////////////setting the comunication with control box of the belt////////////////////////////////////////////

    int cport_nr=16,        /* /dev/ttyS0 (COM1 on windows) */  //16 for v1 USB0, 24 for v2 ACM0
            bdrate=9600;       /* 9600 baud */
    char mode[]={'8','N','1',0}, /* 8 bits, no parity bit and 1 stop bit  */
            str[8][512]; /* 8 strings of long 512 */

    strcpy(str[0], "MD1160"); //right motor activated //MD1080
    strcpy(str[1], "MD1000"); //right motor deactivated
    strcpy(str[2], "MD2160"); //back motor activated
    strcpy(str[3], "MD2000"); //back motor deactivated
    strcpy(str[4], "MI1160"); //left motor activated
    strcpy(str[5], "MI1000"); //left motor deactivated
    strcpy(str[6], "MI2160"); //frontal motor activated
    strcpy(str[7], "MI2000"); //frontal motor deactivated

    if(RS232_OpenComport(cport_nr, bdrate, mode, 0))//if the system can not communicate with the control box, a message will be printed
    {    printf("Can not open comport\n");
    }
    usleep(1500000);//waits for 1.5 seconds

    ////////////////////////////////////setting the grid and the radius of the semicircle that defines dangerous obstalces////////////////////////////////////////////

    size_m_x=FLAGS_g_size_m_x;//size of the grid in x direction, in [m]
    size_m_z=FLAGS_g_size_m_z;//size of the grid in z direction (y axis is parallel to gravity vector), in [m]
    cell_m=FLAGS_g_cell_m;//long of a squared cell that belongs to the grid, in [m]
    th_occupancy_grid=FLAGS_g_th_occupancy_grid; //threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m]

    person_xm=0;//x coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
    person_zm=0;//y coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
    person_angle=0;//orientation of the user. It is updated by reading the camera pose of the ZED Mini
    th_obst_mag=FLAGS_g_th_obst_mag;//threshold in distance that defines if an obstacle is close to the user, generating risk of collision, in [m]

    //////////////////////////////////////////setting RANSAC//////////////////////////////////////////////////////////////////////////////////////////////

    th_ransac=FLAGS_g_th_ransac; //threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]
    max_iter_ransac=FLAGS_g_max_iter_ransac;  //maximum  number of iterations for RANSAC


    //////////////////////////////////////////setting the camera ZED Mini/////////////////////////////////////////////////////////////////////////////

    //see stereolab documentation: https://www.stereolabs.com/docs/
    sl::Camera zed;// Create a ZED camera object

    sl::InitParameters init_params;// Set configuration parameters
    if(load_svo==true){//If load_svo is true a SVO file is loaded
        init_params.input.setFromSVOFile(svo_input_path.c_str());
        //init_params.camera_disable_self_calib = true;
        init_params.camera_disable_self_calib = true;
        std::cout<<"svo_input_path: "<<svo_input_path.c_str()<<std::endl;
    }


    init_params.camera_resolution = sl::RESOLUTION::VGA;//sets camera resolution
    init_params.camera_fps = 15;//sets frames per second
    init_params.coordinate_system = sl::COORDINATE_SYSTEM::IMAGE; //sets coordinate system
    init_params.depth_mode = sl::DEPTH_MODE::QUALITY; //sets depth mode: DEPTH_MODE_ULTRA, DEPTH_MODE_QUALITY, DEPTH_MODE_MEDIUM, DEPTH_MODE_PERFORMANCE
    init_params.coordinate_units = sl::UNIT::METER;//sets units of the data
    init_params.depth_minimum_distance = 0.4 ; // Sets the minimum depth perception distance in [m]


    // Opens the camera for both real ZED Mini or loaded SVO file
    sl::ERROR_CODE err = zed.open(init_params);
    if (err != sl::ERROR_CODE::SUCCESS) {//if an error occurred
        printf("%s\n", toString(err).c_str());
        zed.close();
        std::cout<<"zed closed"<<std::endl;
        return 1; // Quits if an error occurred
    }

    //zed.setDepthMaxRangeValue(3); // Sets the maximum depth perception distance

    sl::PositionalTrackingParameters tracking_parameters; // Enables positional tracking with default parameters
    tracking_parameters.initial_world_transform = sl::Transform::identity();
    err = zed.enablePositionalTracking(tracking_parameters);
    if (err != sl::ERROR_CODE::SUCCESS){//if an error occurred
        printf("%s\n", toString(err).c_str());
        exit(-1);
    }

    sl::RuntimeParameters runtime_parameters;// Sets runtime parameters after opening the camera
    runtime_parameters.sensing_mode = sl::SENSING_MODE::STANDARD; //Sets sensing mode: SENSING_MODE_STANDARD, SENSING_MODE_FILL
    runtime_parameters.confidence_threshold = 90;
    runtime_parameters.texture_confidence_threshold = 100;

    ///////////////////////////////////////////////getting image size, intrinsic parameters and initializing OpenGL/////////////////////////////////////////////////////////////////////////////////////////////////////////////

    downsample_scale=FLAGS_g_downsample_scale; // scale for downsampling the images
    sl::Resolution image_size = zed.getCameraInformation().camera_configuration.resolution; //zed.getResolution();// Retrieves resolution of images: width and height
    new_width = image_size.width/downsample_scale;
    new_height = image_size.height/downsample_scale;
    std::cout<<"image size: "<<new_width<<" "<<new_height<<std::endl;

    sl::CalibrationParameters calibration_params = zed.getCameraInformation().calibration_parameters; //Retrieves intrinsic parameters
    cv::Mat intrinsic_params =(cv::Mat_<float>(3,3) << calibration_params.left_cam.fx/downsample_scale, 0, calibration_params.left_cam.cx/downsample_scale,
                               0, calibration_params.left_cam.fy/downsample_scale, calibration_params.left_cam.cy/downsample_scale,
                               0, 0, 1);
    std::cout<<"intrinsic_params: "<<intrinsic_params<<std::endl;

    init_glut(argc, argv);//initializes the OpenGL parameters

    ///////////////////////////////////////////////////////////while loop//////////////////////////////////////////////////////////////////////////////////////

    //initializes constant memory, allocates device memory and binds texture, in my_kernel_pc
    my_kernel_pc.InitPcGPU(intrinsic_params,new_width,new_height, size_m_x,size_m_z,cell_m, max_iter_ransac, th_ransac, th_occupancy_grid);

    //allocates memory for points and colors
    vertices=(float*)malloc(new_width*new_height*3*sizeof(float));
    colors=(float*)malloc(new_width*new_height*3*sizeof(float));

    // Shares data between sl::Mat and cv::Mat
    sl::Mat image_zed(new_width, new_height, sl::MAT_TYPE::U8_C4, sl::MEM::CPU);//sl::Mat
    cv::Mat image_ocv = slMat2cvMat(image_zed);//cv::Mat
    cv::Mat image_ocvf;//floating cv::Mat
    sl::Mat depth32f(new_width, new_height, sl::MAT_TYPE::F32_C1);//sl::Mat
    cv::Mat depth32fcv;//cv::Mat

    sl::Pose zed_pose;//camera pose

    cv::Mat orMat(4,4,CV_32FC1), /* Matrix that defines the current orientation of the camera (count>1) */
            orRPY(3,1,CV_32FC1), /* Orientation of the camera in roll, pitch, yaw format */
            orMatIni(4,4,CV_32FC1), /* Matrix that defines the initial orientation of the camera (count=1) */
            Tc1c2(4,4,CV_32FC1);
    float beta, beta1, beta2; //inclination of the camera with respect to the horizon, in radians

    int nb_frames;//maximum number of iterations for building the grid
    if(load_svo==true){//if load_svo is true then nb_frames is the number of frames - 2 stored in the SVO file
        nb_frames = zed.getSVONumberOfFrames()-10;
        zed.setSVOPosition(0);
        //nb_frames=200;
    }else{//if load_svo is false then nb_frames is a big number like 100000
        nb_frames=100000;
    }
    std::cout<<"nbframes: "<<nb_frames<<std::endl;

    // Loop until 'e' is pressed. It works when an OpenCV window is active (OpenCV event)
    char key = ' '; //key is initialized as ' '

    int count=0;//counter inside the while loop
    int c2=0, c2max=100;

    while(c2<c2max){
        if (zed.grab(runtime_parameters) == sl::ERROR_CODE::SUCCESS){
            c2=c2+1;
        }else{
            std::cout<<"falla en zed.grab: "<<std::endl;
        }
    }
    start_end_belt_func(cport_nr, str);
    //nb_frames=c2max+1;

    int elapsed_time_sec=0;
    int max_val_time=1800;
    gettimeofday(&tval_before, NULL);
    while (key != 'e' && count<(nb_frames-c2max) && shown==true && elapsed_time_sec<=max_val_time) {//while key is different to 'e' (opencv event)
        //and count is less than nb_frames  and shown is true (opengl event) and elapsed_time_sec<=max_val_time do

        if (zed.grab(runtime_parameters) == sl::ERROR_CODE::SUCCESS) {//If getting data from camera was successful
            // Gets the pose of the left eye of the camera with reference to the world frame
            sl::POSITIONAL_TRACKING_STATE tracking_state = zed.getPosition(zed_pose, sl::REFERENCE_FRAME::WORLD); //it is a quaternion
            if (tracking_state == sl::POSITIONAL_TRACKING_STATE::OK) {
                count=count+1;

                std::cout<<"count: "<<count<<std::endl;

                // Retrieves the color and depth images
                zed.retrieveImage(image_zed, sl::VIEW::LEFT, sl::MEM::CPU);
                zed.retrieveMeasure(depth32f, sl::MEASURE::DEPTH, sl::MEM::CPU);//sl::Mat
                depth32fcv=cv::Mat(depth32f.getHeight(), depth32f.getWidth(), CV_32FC1, depth32f.getPtr<sl::float1>(sl::MEM::CPU));//cv::Mat

                // Displays color and depth images using cv:Mat
                cv::imshow("Depth32", depth32fcv/5);
                cv::imshow("Image", image_ocv);

                // Handles key events for capturing a new key It works when an OpenCV window is active
                key = cv::waitKey(1);

                sl::float3 rote = zed_pose.getEulerAngles();

                ConvertEuler2Mat(rote).copyTo(orMat);
                orMat.at<float>(0,3)=zed_pose.getTranslation().tx;//Gets camera position (x,y,z)
                orMat.at<float>(1,3)=zed_pose.getTranslation().ty;
                orMat.at<float>(2,3)=zed_pose.getTranslation().tz;

                //rotation around x axis of -beta. It moves the optical axis to a plane parallel to the horizon
                beta=rote.x;
                beta1=0;
                beta2=rote.z;

                //rotation around x axis of -beta. It moves the optical axis to a plane parallel to the horizon
                transf_inib1=(cv::Mat_<float>(4,4) << 1, 		0, 		       0,           0,
                              0,		cos (beta),		-1*sin (beta),	0,
                              0, 		sin (beta),		cos (beta),		0,
                              0,		0,			0,			1);//rot x

                transf_inib3=(cv::Mat_<float>(4,4) << cos (beta2),		-1*sin (beta2),	        0,           0,
                              sin (beta2),		cos (beta2),		0,          0,
                              0, 		         0,			   1,			   0,
                              0,		         0,			   0,			1);//rot z

                transf_inib=transf_inib1*transf_inib3;
                transf_inib1.copyTo(transf_inib_vis);
                //Copies the initial transformation to current transformation
                transf_inib.copyTo(transf_current_pc);

                if(count==1){
                    orMat.copyTo(orMatIni);
                    transf_inib_vis.copyTo(transf_current_vis);
                    Transf_accum.create(4,4,CV_32FC1);
                    transf_current_vis.copyTo(Transf_accum); //Stores the first transformation in accumulated transformation
                }else{
                    //Computes the transformation between initial camera pose T_w_i and current camera pose T_w_ci, T_i_ci
                    Tc1c2=orMatIni.inv()*orMat;//T_i_ci

                    //Computes the current transformation referenced to the new global coordinate system WP
                    transf_current_vis=transf_inib_vis*Tc1c2;//T_wp_ci=T_wp_i*T_i_ci

                    //Stores the first transformation in accumulated transformation
                    transf_aux.create(4*(count-1),4,CV_32FC1);
                    //Transf_accum(Range(0,4*(count-1)),Range::all()).copyTo(transf_aux);
                    Transf_accum.copyTo(transf_aux);
                    Transf_accum.create(4*(count),4,CV_32FC1);
                    transf_aux.copyTo(Transf_accum(Range(0,4*(count-1)),Range::all()));
                    transf_current_vis.copyTo(Transf_accum(Range(4*(count-1),4*(count)),Range::all()));
                }

                //computes the orientation and position of the user in the 2D grid
                person_angle=atan2(transf_current_vis.at<float>(2,2),transf_current_vis.at<float>(0,2))*180/3.141592;
                person_xm=transf_current_vis.at<float>(0,3);
                person_zm=transf_current_vis.at<float>(2,3);

                //current orientation of the camera in roll, pitch, yaw format, in radians
                rpy_current_vis=GetRPY2(transf_current_vis(Range(0,3),Range(0,3)));

                //computes vertices in GPU using depth data
                my_kernel_pc.GetVertices(depth32fcv,new_width,new_height);  //*******************************************

                bool isok=true;
                //computes vertices, normals, the plane that best fits to data and builds an occupancy grid
                my_kernel_pc.GetVerticesNorms(transf_current_pc(Range(0,3),Range(0,4)),new_width,new_height,count,
                                              person_xm, person_zm, person_angle, th_obst_mag, downsample_scale, reactiveNav, isok);  //*****************************************

                //controls the activation of the vibrating motors of the belt
                belt_func(reactiveNav, cport_nr, str);

            }//end tracking ok
        }//end if grab******


        if(draw_grid==true){//If true then the occupancy 2D grid is shown. It is inside the while loop
            glutDisplayFunc(displayOccu);
        }else{//If false the local point cloud is shown. It is inside the while loop
            glutDisplayFunc(display);
        }
        usleep(1000);
        glutMainLoopEvent();//shows the point cloud or the occupancy 2D grid. It is inside the while loop
        glutPostRedisplay();

        gettimeofday(&tval_after, NULL);
        timersub(&tval_after, &tval_before, &tval_result);
        //printf("Time elapsed: %ld.%06ld\n", (long int)tval_result.tv_sec, (long int)tval_result.tv_usec);
        elapsed_time_sec=(int)tval_result.tv_sec;
        //std::cout << "elapsed_time_sec: " << elapsed_time_sec << std::endl;
    }//end while******

    shown=true;
    //it shows the resulting occupancy 2D grid or the last local point cloud, after finishing the while loop
    while(key != 'q' && shown==true && elapsed_time_sec<=max_val_time){
        if(draw_grid==true){//If true then the occupancy 2D grid is shown. It is after the iterations fo the while loop
            glutDisplayFunc(displayOccu);
        }else{
            glutDisplayFunc(display);//If false the local point cloud is shown. It is after the iterations fo the while loop
        }
        usleep(1000);
        glutMainLoopEvent();//shows the point cloud or the occupancy 2D grid. It is after the iterations fo the while loop
        glutPostRedisplay();
        // Handles key events for capturing a new key. It works when an OpenCV window is active
        key = cv::waitKey(10);
    }

    //this sequence of acativations of the motors are for clossing the port successfully
    belt_func(reactiveNav, cport_nr, str);
    usleep(500000);
    belt_func(reactiveNav, cport_nr, str);
    usleep(500000);
    belt_func(reactiveNav, cport_nr, str);
    usleep(500000);
    belt_func(reactiveNav, cport_nr, str);
    usleep(500000);

    start_end_belt_func(cport_nr, str);
    usleep(500000);
    RS232_CloseComport(cport_nr);//closes the port

    // Disables positional tracking and close the camera
    zed.disablePositionalTracking();
    zed.close();

    LOG(INFO) << "Reached the end of the App.";

    //frees host and device memory an unbinds texture memory
    my_kernel_pc.Unbind();
    return 0;
}//end main****

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Converts sl::Mat to cv::Mat
cv::Mat slMat2cvMat(sl::Mat &input) {
    int cv_type = -1;
    switch (input.getDataType()) {
    case sl::MAT_TYPE::F32_C1: cv_type = CV_32FC1; break;
    case sl::MAT_TYPE::F32_C2: cv_type = CV_32FC2; break;
    case sl::MAT_TYPE::F32_C3: cv_type = CV_32FC3; break;
    case sl::MAT_TYPE::F32_C4: cv_type = CV_32FC4; break;
    case sl::MAT_TYPE::U8_C1: cv_type = CV_8UC1; break;
    case sl::MAT_TYPE::U8_C2: cv_type = CV_8UC2; break;
    case sl::MAT_TYPE::U8_C3: cv_type = CV_8UC3; break;
    case sl::MAT_TYPE::U8_C4: cv_type = CV_8UC4; break;
    default: break;
    }
    // Since cv::Mat data requires a uchar* pointer, we get the uchar1 pointer from sl::Mat (getPtr<T>())
    // cv::Mat and sl::Mat will share a single memory structure
    return cv::Mat(input.getHeight(), input.getWidth(), cv_type, input.getPtr<sl::uchar1>(sl::MEM::CPU));
}

//displays help in console
void printHelp() {
    std::cout << " Press 's' to save Side by side images" << std::endl;
    std::cout << " Press 'p' to save Point Cloud" << std::endl;
    std::cout << " Press 'd' to save Depth image" << std::endl;
    std::cout << " Press 'm' to switch Point Cloud format" << std::endl;
    std::cout << " Press 'n' to switch Depth format" << std::endl;
}

//converts transformation matrix to rpy format, version 1
cv::Mat GetRPY(cv::Mat cameraRot){
    cv::Mat rpy(3,1,CV_32FC1);
    float phi=atan2(cameraRot.at<float>(1,0),cameraRot.at<float>(0,0));

    rpy.at<float>(0,0)=atan2((cameraRot.at<float>(0,2)*sin(phi)-cameraRot.at<float>(1,2)*cos(phi)),(-cameraRot.at<float>(0,1)*sin(phi)+
                                                                                                    cameraRot.at<float>(1,1)*cos(phi)));
    rpy.at<float>(1,0)=atan2(-cameraRot.at<float>(2,0),(cameraRot.at<float>(0,0)*cos(phi)+cameraRot.at<float>(1,0)*sin(phi)));
    rpy.at<float>(2,0)=phi;
    return rpy;
}

//converts transformation matrix to rpy format, version 2
cv::Mat GetRPY2(cv::Mat cameraRot){
    cv::Mat rpy(3,1,CV_32FC1);
    float phi=atan2(cameraRot.at<float>(1,0),cameraRot.at<float>(0,0));

    rpy.at<float>(0,0)=atan2(cameraRot.at<float>(2,1),cameraRot.at<float>(2,2));
    rpy.at<float>(1,0)=atan2(-cameraRot.at<float>(2,0),sqrt(cameraRot.at<float>(2,1)*cameraRot.at<float>(2,1)+cameraRot.at<float>(2,2)*cameraRot.at<float>(2,2)));
    rpy.at<float>(2,0)=phi;
    return rpy;
}

//converts quaternion to matrix
cv::Mat ConvertQuat2Mat(float orqx, float orqy, float orqz, float orqw){
    cv::Mat rotMat(4,4,CV_32FC1);

    rotMat=(cv::Mat_<float>(4,4) << 1-2*orqy*orqy-2*orqz*orqz, 	2*orqx*orqy-2*orqz*orqw, 	2*orqx*orqz+2*orqy*orqw, 0,
            2*orqx*orqy+2*orqz*orqw,	1-2*orqx*orqx-2*orqz*orqz,	2*orqy*orqz-2*orqx*orqw, 0,
            2*orqx*orqz-2*orqy*orqw, 	2*orqy*orqz+2*orqx*orqw,	1-2*orqx*orqx-2*orqy*orqy, 0,
            0,				0,				0,				1);

    /* rotMat=(cv::Mat_<float>(4,4) << pow(orqx,2)+pow(orqy,2)-pow(orqz,2)-pow(orqw,2), 2*(orqy*orqz-orqx*orqw), 2*(orqy*orqw+orqx*orqz), 0,
                                   2*(orqy*orqz+orqx*orqw), pow(orqx,2)+pow(orqz,2)-pow(orqy,2)-pow(orqw,2), 2*(orqz*orqw-orqx*orqy), 0,
                                    2*(orqy*orqw-orqx*orqz), 2*(orqz*orqw+orqx*orqy), pow(orqx,2)+pow(orqw,2)-pow(orqy,2)-pow(orqz,2), 0,
                                    0,                           0,                     0,                          1);
*/
    return rotMat;
}

cv::Mat ConvertEuler2Mat(sl::float3 rote){
    cv::Mat tmat(4,4,CV_32FC1);
    float ca=cos(rote.z), sa=sin(rote.z);
    float cb=cos(rote.y), sb=sin(rote.y);
    float cg=cos(rote.x), sg=sin(rote.x);

    tmat=(cv::Mat_<float>(4,4) <<  ca*cb, ca*sb*sg-sa*cg, ca*sb*cg+sa*sg, 0,
          sa*cb, sa*sb*sg+ca*cg, sa*sb*cg-ca*sg, 0,
          -sb,   cb*sg,          cb*cg, 0,
          0,  0,  0,  1);
    return tmat;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//initializes the OpenGL parameters
void init_glut(int argc, char** argv) {

    glutInit(&argc, argv);
    glutInitWindowSize(900, 900);
    glutInitWindowPosition(0, 0);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);   // display mode

    glutCreateWindow("AssistNavBP V1.1");

    glEnable(GL_DEPTH_TEST);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);

    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glClearColor(0, 0, 0, 0.5f);// background color
    glClearStencil(0);// clear stencil buffer
    glClearDepth(1.0f);// 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);

    initLights();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouseFunc);
    glFlush();
}


//draws the camera as a mobile coordinate system in OpenGL
void draw_vehicle(void) {
    glBegin(GL_LINES);
    glColor3f(1.0f, 1.0f, 0.0f);//yellow
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(3.0, 0.0, 0.0);
    glColor3f(0.0f, 1.0f, 0.0f);//green
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 3.0, 0.0);
    glColor3f(0.0f, 1.0f, 1.0f); //cyan
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 5.0);
    glEnd();
}


//manages the keyboard events
void keyboard(unsigned char key, int x, int y) {
    double delta = 10.0 * 3.1415 / 180; //angular change for point of view in the 3D environment for each time "w", "s", "a", or "d" keys are pressed
    double delta_dis=0.5; //change in distance for point of view in the 3D environment for each time "i" or "k" keys are pressed
    switch (key) {

    case 'w':
        ele += delta; //elevation +
        break;

    case 's':
        ele -= delta; //elevation -
        break;

    case 'a':
        az += delta; //azimuth +
        break;

    case 'd':
        az -= delta; //azimuth -
        break;

    case 'i':
        dis += delta_dis; //distance +
        break;

    case 'k':
        dis -= delta_dis; //distance -
        if(dis<-ini_dis+0.1){
            dis=-ini_dis+0.1;
        }
        break;

    case 'v':
        if(draw_grid == true)//If true then the occupancy 2D grid is shown. Otherwise, the local point cloud is shown.
            draw_grid = false;
        else
            draw_grid =true;
        break;

    case (27):
        shown = false;
        break;

    case 'n':
        if (flag_color == true)//If true then the point cloud is drawn with color associated to RGB data. Otherwise, it is drawn with color associated to normal vectors                .
            flag_color = false;
        else
            flag_color = true;
        break;

    case 'f':
        if (flag_activePoints == true)//If true then the point cloud is drawn with all the local points. Otherwise, only points that belong to the floor are drawn.
            flag_activePoints = false;
        else
            flag_activePoints = true;
        break;
    }
}


//manages the mouse events for zooming
void mouseFunc(int button, int state, int x, int y) {
}


//idleFunc of OpenGL
void idleFunc(void) {
    glutPostRedisplay();
}


//reshapes the window (fixes the window's size) and its content for OpenGL
void reshape(int width, int height) {
    glViewport(0, 0, width, height);
}


//displays the local point cloud and the camera pose, according to the view mode
void display() {

    int height=new_height,width=new_width;//image size

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//clears window

    glMatrixMode(GL_PROJECTION);//mode: projection
    glLoadIdentity();
    gluPerspective(fov, (double) width / (double) height, znear, zfar);

    glMatrixMode(GL_MODELVIEW);//mode: modelview
    glLoadIdentity();

    //computes the pose of the virtual camera in OpenGL according to distance, azimuth and elevation. These parameters can be modified by pressing keys
    tyc = (dis+ini_dis) * sin(ele + ini_ele);
    tzc = (dis+ini_dis) * cos(ele + ini_ele) * sin(az + ini_az);
    txc = (dis+ini_dis) * cos(ele + ini_ele) * cos(az + ini_az);
    gluLookAt(txc, tyc, tzc, 0, 0, 1.0, 0, 1, 0);

    glRotatef(-180, 1, 0, 0);//rotates the camera

    glPushMatrix();//stores the matrix that defines the pose of the virtual camera in OpenGL

    //std::cout<<"n_vert in display: "<<n_vert<<std::endl;
    if(n_vert>0){//if there are points then the system draws them
        draw_points();   //************************
    }

    //moves the camera, represented as a coordinate system, to the current camera pose
    glTranslatef(transf_current_vis.at<float>(0, 3), transf_current_vis.at<float>(1, 3), transf_current_vis.at<float>(2, 3));//-1.0
    glRotatef((rpy_current_vis.at<float>(2, 0))*(180 / 3.1415), 0, 0, 1); //rotation around z axis
    glRotatef((rpy_current_vis.at<float>(1, 0))*(180 / 3.1415), 0, 1, 0); //rotation around y axis
    glRotatef((rpy_current_vis.at<float>(0, 0))*(180 / 3.1415), 1, 0, 0); //rotation around x axis

    glScaled(0.5, 0.5, 0.5);
    draw_vehicle();//draws the camera as a mobile coordinate system in OpenGL
    glScaled(2, 2, 2);

    glColor3f(1.0f, 1.0f, 0.0f);
    char st[]= "X_cam";
    renderBitmapString(0.3,0,0,GLUT_BITMAP_TIMES_ROMAN_10, st);//displays the text: "X_cam" on the 3D scene in OpenGL

    glColor3f(0.0f, 1.0f, 0.0f);
    char st2[]= "Y_cam";
    renderBitmapString(0,0.3,0,GLUT_BITMAP_TIMES_ROMAN_10, st2);//displays the text: "Y_cam" on the 3D scene in OpenGL

    glPopMatrix();//loads the matrix that defines the pose of the virtual camera in OpenGL

    //flush drawing routines to the window
    glutSwapBuffers();
    glFlush();
}


//displays the occupancy 2D grid
void displayOccu() {

    int height=new_height,width=new_width;//image size

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//clears window

    glMatrixMode(GL_PROJECTION);//mode: projection
    glLoadIdentity();
    gluPerspective(fov, 1, znear, zfar);

    glMatrixMode(GL_MODELVIEW);//mode: modelview
    glLoadIdentity();
    gluLookAt(0, 0, 10+dis+ini_dis, 0, 0, -10, 0, 1, 0);//pose of the virtual camera in OpenGL

    float size_c_x=size_m_x/cell_m;//number of cells of the grid in x direction
    float size_c_z=size_m_z/cell_m;//number of cells of the grid in z direction
    draw_Occu_grid2(size_c_x, size_c_z, cell_m);//draws the occupancy 2D grid*******************

    draw_trajectory();//draws the trajectory of the camera (user) in the occupancy 2D grid

    //flush drawing routines to the window */
    glutSwapBuffers();
    glFlush();
}


//draws a point cloud computed from local depth data
void draw_points(){

    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);// activates vertex array

    glPointSize(1.0);

    glColorPointer(3,GL_FLOAT, 0, colors); // color of the points
    glVertexPointer(3, GL_FLOAT, 0, vertices); // points to be drawn

    glDrawArrays(GL_POINTS, 0, n_vert);

    glDisableClientState(GL_VERTEX_ARRAY); // deactivate vertex array
    glDisableClientState(GL_COLOR_ARRAY); // deactivate color array
}



//displays text on the 3D scene in OpenGL
void renderBitmapString(float x,float y,float z,void *font,	char *string) {
    char *c;
    c=string;
    glRasterPos3f(x, y,z);
    for (int count=0; count <= 8; count++) {
        glutBitmapCharacter(font, c[count]);
    }
}


//manages the lights of the scene in OpenGL
void initLights()
{
    // set up light colors (ambient, diffuse, specular)
    GLfloat lightKa[] = {.6f, .6f, .6f, 1.0f};  // ambient light
    GLfloat lightKd[] = {.7f, .7f, .7f, 1.0f};  // diffuse light
    GLfloat lightKs[] = {1, 1, 1, 1};           // specular light
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

    // position the light
    float lightPos[4] = {0, 0, -10, 1}; // positional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glEnable(GL_LIGHT0);// enables each light source after configuration
}


//draws a global occupancy 2d grid by merging local depth data
void draw_Occu_grid2(float size_c_x, float size_c_z, float cell_m){

    int ixy;
    for(int ix=0;ix<(int)size_c_x;ix++){//for each cell of the grid
        for(int iy=0;iy<(int)size_c_z;iy++){
            ixy=ix+iy*size_c_x;
            if(my_kernel_pc.OccuFW_host[ixy]==1){//if the cell is free
                glColor3f(0.0,1.0,0.0);//green
                glRectf((ix-(size_c_x/2))*cell_m,((size_c_z/2)-iy)*cell_m,(ix+1-(size_c_x/2))*cell_m,((size_c_z/2)-(iy+1))*cell_m);
            }else{
                if(my_kernel_pc.OccuOW_host[ixy]==1){//if the cell is occupied
                    glColor3f(1.0,0.0,0.0);//red
                    glRectf((ix-(size_c_x/2))*cell_m,((size_c_z/2)-iy)*cell_m,(ix+1-(size_c_x/2))*cell_m,((size_c_z/2)-(iy+1))*cell_m);
                }
            }
        }
    }

    //draws a point in the position of the user in the occupancy 2D grid
    glPointSize(4.0);
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_POINTS);
    glVertex2f(person_xm, person_zm);
    glEnd();

    //draws the semicircle that defines possible collisions (in white)
    glColor3f(1.0, 1.0, 1.0);
    DrawCircle(person_xm, person_zm, th_obst_mag, 20, person_angle);

    //coordinates of the end point of bearing line that defines orientation of the user in the occupancy 2D grid (yellow line)
    float bearingfpx=person_xm+th_obst_mag*cos((person_angle*3.141592/180));
    float bearingfpz=person_zm+th_obst_mag*sin((person_angle*3.141592/180));

    //line for defining the bearing
    glLineWidth(4.0f);
    glColor3f(0.0, 1.0, 1.0);//cyan
    glBegin(GL_LINES);
    glVertex2f(person_xm, person_zm);
    glVertex2f(bearingfpx, bearingfpz);
    glEnd();
    glLineWidth(1.0f);

    //reactive navigation
    draw_reactiveNav_func(reactiveNav);
}


//draws the trajectory of the user in the occupancy 2D grid
void draw_trajectory() {
    int scale = 1;
    glColor3f(0.0f, 0.0f, 1.0f); //blue

    glLineWidth(2.0f);
    glBegin(GL_LINES);
    for (int ii = 0; ii < (int)(Transf_accum.rows/4)-1; ii++) {//for each segment of the trajectory
        glVertex2f(Transf_accum.at<float>(ii*4, 3) * scale, Transf_accum.at<float>(ii*4+2, 3) * scale);
        glVertex2f(Transf_accum.at<float>((ii+1)*4, 3) * scale, Transf_accum.at<float>((ii+1)*4+2, 3) * scale);
    }
    glEnd();
}


//draws a circle in cx,cz, with radius r. This circle defines posible collisions
void DrawCircle(float cx, float cz, float r, int num_segments, float person_angle)
{
    glBegin(GL_LINE_LOOP);
    for(int ii = 0; ii <= num_segments; ii++){
        float theta = 1.0f * 3.1415926f * float(ii) / float(num_segments)+person_angle*3.141592/180-3.141592/2;//gets the current angle

        float x = r * cosf(theta);//calculates the x component
        float z = r * sinf(theta);//calculates the z component

        glVertex2f(x + cx, z + cz);//output vertex
    }
    glEnd();

    for(int ii = 0; ii <= 6; ii++){//splits the semicircle in six cones

        float theta = 1.0f * 3.1415926f * float(ii) / float(6)+person_angle*3.141592/180-3.141592/2;//gets the current angle

        float x = r * cosf(theta);//calculates the x component
        float y = r * sinf(theta);//calculates the y component
        glBegin(GL_LINES);
        glVertex2f(cx, cz);
        glVertex2f(x + cx, y + cz);
        glEnd();
    }
}


//function for reactive navigation. It uses the number of occupied cells inside each cone of 30 degrees of a semicircle for navigating without collisions
void draw_reactiveNav_func(uint *reactiveNav){

    float ax=-5,ay=5;

    int th_occ_cell=0;
    //points on the upper left that represents a motor. white -> deactivated, red -> activated
    glPointSize(8.0);
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_POINTS);
    glVertex2f(-8+ax, 8+ay);//motor in the back. this motor will be used in the second stage of the project

    if(reactiveNav[4]>th_occ_cell || reactiveNav[5]>th_occ_cell){//obstacles in the cones from 120 to 180 degrees
        glColor3f(1.0, 0.0, 0.0);//red
    }else{glColor3f(1.0, 1.0, 1.0);}//white
    glVertex2f(-6+ax, 8+ay);//motor in the left

    if(reactiveNav[2]>th_occ_cell || reactiveNav[3]>th_occ_cell){//obstacles in the cones from 60 to 120 degrees
        glColor3f(1.0, 0.0, 0.0);//red
    }else{glColor3f(1.0, 1.0, 1.0);}//white
    glVertex2f(-4+ax, 8+ay);//motor in the middle

    if(reactiveNav[0]>th_occ_cell || reactiveNav[1]>th_occ_cell){//obstacles in the cones from 0 to 60 degrees
        glColor3f(1.0, 0.0, 0.0);//red
    }else{glColor3f(1.0, 1.0, 1.0);}//white
    glVertex2f(-2+ax, 8+ay);//motor in the right
    glEnd();

    glColor3f(1.0f, 1.0f, 0.0f);
    char st1[]= "back    ";
    renderBitmapString(-8.3+ax,7.5+ay,0,GLUT_BITMAP_TIMES_ROMAN_10, st1);//displays the text: "back" on the upper left area of the window of OpenGL
    char st2[]= "left    ";
    renderBitmapString(-6.3+ax,7.5+ay,0,GLUT_BITMAP_TIMES_ROMAN_10, st2);//displays the text: "left" on the upper left area of the window of OpenGL
    char st3[]= "middle    ";
    renderBitmapString(-4.3+ax,7.5+ay,0,GLUT_BITMAP_TIMES_ROMAN_10, st3);//displays the text: "middle" on the upper left area of the window of OpenGL
    char st4[]= "right    ";
    renderBitmapString(-2.3+ax,7.5+ay,0,GLUT_BITMAP_TIMES_ROMAN_10, st4);//displays the text: "right" on the upper left area of the window of OpenGL

    glFlush();
}


//controls the activation of the vibrating motors of the belt
void belt_func(uint *reactiveNav, int cport_nr, char str[][512]){

    if(reactiveNav[4]>0 || reactiveNav[5]>0){//obstacles in 120-180
        RS232_cputs(cport_nr, str[4]);//activates the motor on the left
    }else{RS232_cputs(cport_nr, str[5]);}//deactivates the motor on the left

    if(reactiveNav[2]>0 || reactiveNav[3]>0){//obstacles in 60-120
        RS232_cputs(cport_nr, str[2]);//activates the motor on the middle //6
    }else{RS232_cputs(cport_nr, str[3]);}//deactivates the motor on the middle  //7

    if(reactiveNav[0]>0 || reactiveNav[1]>0){//obstacles in 0-60
        RS232_cputs(cport_nr, str[0]);//activates the motor on the right
    }else{RS232_cputs(cport_nr, str[1]);}//deactivates the motor on the right
}

//sequence of vibrations for informing to the user about the start and end of the experiment
void start_end_belt_func(int cport_nr, char str[][512]){

    for(int ib=1;ib<3;ib++){
        RS232_cputs(cport_nr, str[1]);
        RS232_cputs(cport_nr, str[3]);
        RS232_cputs(cport_nr, str[5]);
        usleep(150000);
        RS232_cputs(cport_nr, str[0]);
        RS232_cputs(cport_nr, str[2]);
        RS232_cputs(cport_nr, str[4]);
        usleep(150000);
        RS232_cputs(cport_nr, str[1]);
        RS232_cputs(cport_nr, str[3]);
        RS232_cputs(cport_nr, str[5]);
        usleep(150000);
        RS232_cputs(cport_nr, str[0]);
        RS232_cputs(cport_nr, str[2]);
        RS232_cputs(cport_nr, str[4]);
        usleep(150000);
        RS232_cputs(cport_nr, str[1]);
        RS232_cputs(cport_nr, str[3]);
        RS232_cputs(cport_nr, str[5]);
        usleep(150000);
    }
}

//date and time
std::string datetime()
{
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,80,"%d-%m-%Y_%H-%M-%S",timeinfo);
    return std::string(buffer);
}

