//============================================================================
// Name        : kernel_pc.h
// Authors     : Andres Diaz - Sixto Campaña - Eduardo Caicedo
// Version     : 1.1
// Copyright   : All rights reserved
// Description : AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1
//============================================================================


#ifndef KERNEL_PC_H
#define KERNEL_PC_H

#include <iostream>
#include <eigen3/Eigen/Dense>

#include <stdint.h>
#include <string>
#include <stdio.h>

#include <thrust/device_vector.h>
#include <thrust/scan.h>
#include <thrust/reduce.h>
#include <thrust/extrema.h>
#include <thrust/execution_policy.h>

#include <stdlib.h>     /* srand, rand */

#include <sys/time.h>
#include <time.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

static const cudaChannelFormatDesc chd_float2 = cudaCreateChannelDesc<float>();
static const cudaChannelFormatDesc chd_float4 = cudaCreateChannelDesc<float4>();

using namespace std;
using namespace cv;
using namespace Eigen;
using Eigen::MatrixXf;

/**
 * @brief The kernel_pc class contains structures and functions that initialize variables, allocate memory, compute local vertices,
 * compute global vertices, global normal vectors,
 * make an initial floor segmentation, refine the initial segmentation with RANSAC, build an occupancy 2D grid and
 * carry out reactive navigation. RANSAC is carried out only once, for the first iteration.
 */
class kernel_pc
{
public:
    /**
     * @brief kernel_pc is the constructor of the class kernel_pc
     */
    kernel_pc(void);

    /**
      * @brief ~kernel_pc is the destructor of the class kernel_pc
      */
    ~kernel_pc();

    /**
     * @brief The Vertex struct stores the coordinates (x,y,z) of a vertex. w is used as flag some times
     */
    struct Vertex{
        float x, /**< X coordinate of a vertex */
        y, /**< Y coordinate of a vertex */
        z, /**< Z coordinate of a vertex */
        w; /**< it is used as flag some times */
    };

    /**
     * @brief The Normal struct stores the coordinates of a unit normal vector. w is used as flag some times
     */
    struct Normal{
        float x, /**< X coordinate of a normal */
        y, /**< Y coordinate of a normal */
        z, /**< Z coordinate of a normal */
        w; /**< it is used as flag some times */
    };

    size_t pitch2; /**<  pitch for allocation, in cudaMallocPitch */

    Vertex *verticesK_gpu; /**< device pointer for local vertices in k */

    Vertex *verticesW_gpu; /**< device pointer for global vertices in k */
    Normal *normalsW_gpu; /**< device pointer for global normal vectors in k */
    Vertex *verticesW_host; /**< host pointer for global vertices */
    Normal *normalsW_host;  /**< host pointer for global normal vectors */

    float *depth_gpu; /**< device pointer for depthmaps coming from the camera ZED Mini */

    uint *compactedDataId_host; /**< host pointer for indices of points for an initial segmentation of the floor */

    float *verticesY_gpu; /**< device pointer for Y component of points */

    uint *verFlags_gpu; /**< device pointer for flag that is set to 1 if a point (vertex) has a normal vector parallel to the gravity vector and if its y component is greater than a threshold */

    uint *verFlagsScan_gpu; /**< device pointer for indice that increases for each new point with flag verFlags_gpu=1 */

    uint activePoints; /**< number of points that have a normal vector parallel to the gravity vector and their y-component is greater than a threshold */

    uint *compactedDataId_gpu; /**< device pointer for indices of points for an initial segmentation of the floor */

    int max_iter_ransac; /**< maximum number of iterations for carrying out RANSAC */
    int max_range, /**< number of points of the initial floor segmentation. It is used as the upper limit for computing random numbers for RANSAC */
    min_range; /**< lower limit for computing random numbers for RANSAC */
    int *randNum_host; /**< random numbers for RANSAC, in host memory */
    float th_ransac; /**< threshold for defining if a point is close to a plane, in [m] */

    float person_xm; /**< x coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini */
    float person_zm; /**< z coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini */
    float person_angle; /**< orientation of the user. It is updated by reading the camera pose of the ZED Mini */
    float th_obst_mag; /**< threshold for the distance from an obstacle to the user */

    float size_m_x, /**< size of the grid in x direction, in [m] */
    size_m_z, /**< size of the grid in z direction, in [m] */
    cell_m, /**< long of a squared cell that belongs to the grid, in [m] */
    size_c_x, /**< number of cells of the grid in x direction */
    size_c_z; /**< number of cells of the grid in z direction */

    float th_outlier_y; /**< threshold that defines if a point is an outlier by evaluating its y coordinate, in [m] */
    float th_normal_y; /**< threshold that defines if a normalized normal vector is parallel to the gravity vector */
    float part_max_val; /**< part of the maximum value in y coordinate that defines if a point is close to the floor */
    float min_dis_user;  /**< minimum distance over which the occupancy 2D grid is updated, in [m] */
    float th_occupancy_grid; /**< threshold for defining if a grid is free or occupied by evaluating the y component of the points */

    int *randNum_gpu; /**< device pointer for random number for reading a random point from the initial segmentation of the floor */

    uint *diff_lth_gpu;	/**< it is used to compute the equation that best fits to points from the initial segmentation of the floor */

    Vertex *verticesWnew_gpu; /**< device pointers for global vertices in k+1 */
    Vertex *verticesWnew_host; /**< host pointers for global vertices in k+1 */

    uint *OccuF_gpu; /**< 2D grid in device memory that stores the number of free points (points with y-coordinate over a threshold) proyected over each cell. */
    uint *OccuO_gpu; /**< 2D grid  in device memory that stores the number of occupied points (points with y-coordinate under a threshold) proyected over each cell. */
    uint *OccuO_host;  /**< 2D grid  in host memory that stores the number of occupied points (points with y-coordinate under a threshold) proyected over each cell. */
    uint *OccuF_host;  /**< 2D grid in host memory that stores the number of free points (points with y-coordinate over a threshold) proyected over each cell. */


    uint *OccuFW_gpu; /**< 2D grid in device memory that stores cells with 1 if a cell is free and 0 otherwise */
    uint *OccuOW_gpu; /**< 2D grid in device memory that stores cells with 1 if a occupied is occupied and 0 otherwise */
    uint *OccuOW_host; /**< 2D grid in host memory that stores cells with 1 if a occupied is occupied and 0 otherwise */
    uint *OccuFW_host; /**< 2D grid in host memory that stores cells with 1 if a cell is free and 0 otherwise */

    uint *reactnav_gpu; /**< array in device memory of six elements with the number of occupied cells inside each cone of the semicircle around the user */
    uint *reactnav_host; /**< array in host memory of six elements with the number of occupied cells inside each cone of the semicircle around the user */

    float max_val, /**< maximum value in Y axis */
        d_plane;  /**< d parameter of the most concordant plane (ax+by+cz+d=0) */

    float alpha, /**< angle alpha for moving the plane of the floor to the plane XZ (y coordinate equal to zero) */
        beta; /**< angle beta for moving the plane of the floor to the plane XZ (y coordinate equal to zero) */

    kernel_pc::Normal normal_plane; /**< the normal vector of the most concordant plane (ax+by+cz+d=0) */
    kernel_pc::Vertex verR1; /**< random point R1 from the initial segmentation of the floor */

    /**
     * @brief InitPcGPU initializes the variables, allocates texture memory, host memory and device memory for the data
     * @param K intrinsic matrix of the camera ZED Mini
     * @param cols number of columns of the image
     * @param rows number of rows of the image
     * @param Dsize_m_x size of the grid in x direction, in [m]
     * @param Dsize_m_z size of the grid in y direction, in [m]
     * @param Dcell_m long of a squared cell that belongs to the grid, in [m]
     * @param Dmax_iter_ransac maximum  number of iterations for RANSAC
     * @param Dth_ransac threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]
     * @param Dth_occupancy_grid threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m]
     */
    void InitPcGPU(Mat K, int cols, int rows, float Dsize_m_x, float Dsize_m_z, float Dcell_m, int Dmax_iter_ransac, float Dth_ransac, float Dth_occupancy_grid);

    /**
     * @brief GetVertices initializes device memory with depth data and calls the kernel my_kernelVK that computes on GPU local vertices for the current iteration
     * @param depth depth data captured with the camera ZED Mini
     * @param cols number of columns of the image
     * @param rows number of rows of the image
     */
    void GetVertices(Mat depth, int cols, int rows);

    /**
     * @brief kernel_pc::GetVerticesNorms computes global vertices, global normal vectors, makes an initial floor segmentation, refines the initial
     * segmentation with RANSAC, builds an occupancy 2D grid and carries out reactive navigation. RANSAC is carried out only once, for the first iteration. It calls
     * the kernels my_kernelVN, my_kernelFloorND, compactData, kernel_ransac, kernel_Occupancy2DGrid, kernel_Occupancy and kernel_reactive_wandering
     * @param Twc current transformation of the user
     * @param cols number of columns of the image
     * @param rows number of rows of the image
     * @param count current iteration of the while loop in the main file
     * @param Dperson_xm x coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
     * @param Dperson_zm z coordinate of the current pose of the user in the 2D grid, in [m]. It is updated by reading the camera pose of the ZED Mini
     * @param Dperson_angle orientation of the user. It is updated by reading the camera pose of the ZED Mini
     * @param Dth_obst_mag threshold for the distance from an obstacle to the user
     * @param downsample_scale scale for downsampling the images
     * @param reactiveNav array of six elements with the number of occupied cells inside each cone of the semicircle around the user
     */
    void GetVerticesNorms(Mat Twc, int cols, int rows, int count, float Dperson_xm, float Dperson_zm, float Dperson_angle, float Dth_obst_mag,
                          int downsample_scale, uint *reactiveNav, bool &isok);

    /**
     * @brief Unbind frees host and device memory and unbinds texture memory
     */
    void Unbind();

    /**
     * @brief GetSums sums 1s for defining the parameters with most concordant points (equation of the plane that best fits to data)
     * @param sumPointsParamsPlane sum of concordant points for eacha evaluated equation
     * @param activePoints number of points that have a normal vector parallel to the gravity vector and their y-component is greater than a threshold
     * @param max_iter_ransac maximum number of iterations for carrying out RANSAC
     */
    void GetSums(MatrixXf &sumPointsParamsPlane, int activePoints, int max_iter_ransac);

    /**
     * @brief get_VN_plane gets the normal and d parameter of the most concordant plane (ax+by+cz+d=0)
     * @param normal_plane the normal vector of the most concordant plane (ax+by+cz+d=0)
     * @param d_plane d parameter of the most concordant plane (ax+by+cz+d=0)
     * @param verR1 random point R1 from the initial segmentation of the floor
     * @param compactedDataId_host host pointer for indices of points for an initial segmentation of the floor
     * @param randNum_host random numbers for RANSAC, in host memory
     * @param maxRow indices of the points that produced the most concordant parameters of the plane
     */
    void get_VN_plane(kernel_pc::Normal &normal_plane, float &d_plane, kernel_pc::Vertex &verR1, uint *compactedDataId_host, int *randNum_host, int maxRow);

    /**
     * @brief get_transform gets transformation matrix using alpha and beta. It moves the plane of the floor to the plane XZ (y coordinate equal to zero)
     * @param transf transformation matrix that moves the plane of the floor to the plane XZ (y coordinate equal to zero)
     * @param alpha angle alpha for moving the plane of the floor to the plane XZ (y coordinate equal to zero)
     * @param beta angle beta for moving the plane of the floor to the plane XZ (y coordinate equal to zero)
     * @param verR1 random point R1 from the initial segmentation of the floor
     * @param d_plane d parameter of the most concordant plane (ax+by+cz+d=0)
     */
    void get_transform(Matrix4f &transf, float alpha, float beta, kernel_pc::Vertex verR1, float d_plane, kernel_pc::Normal normal_plane);

private:
};


#endif //KERNEL_PC_H


