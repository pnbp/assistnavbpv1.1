# find glog includes
find_path( GLog_INCLUDE_DIR glog/logging.h ${CMAKE_BINARY_DIR}/contrib/glog/include /opt/local/include /usr/local/include /usr/include )

# find glog library
find_library( GLog_LIBRARY NAMES glog PATHS ${CMAKE_BINARY_DIR}/contrib/glog/lib/ /opt/local/lib /usr/local/lib /usr/lib )

# glog requires gflags.
# Forward the QUIET / REQUIRED options.
if (GLog_FIND_QUIETLY)
  find_package(GFlags QUIET)
elseif (GLog_FIND_REQUIRED)
  find_package(GFlags REQUIRED)
else ()
  find_package(GFlags)
endif()

# handle the QUIETLY and REQUIRED arguments and set GLog_FOUND to TRUE if
# all listed variables are TRUE
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GLog DEFAULT_MSG GLog_INCLUDE_DIR GLog_LIBRARY GFlags_INCLUDE_DIRS GFlags_LIBRARIES )

set( GLog_INCLUDE_DIRS ${GLog_INCLUDE_DIR} ${GFlags_INCLUDE_DIRS} )
set( GLog_LIBRARIES ${GLog_LIBRARY} ${GFlags_LIBRARIES} )

mark_as_advanced( GLog_INCLUDE_DIR GLog_LIBRARY )

# Hide unused local typedef warnings from within glog if compiler supports it
# (for Xcode 7.0+).
include(CheckCXXCompilerFlag)
check_cxx_compiler_flag("-Wunused-local-typedef" HAS_UNUSED_LOCAL_TYPEDEF_FLAG)
if (HAS_UNUSED_LOCAL_TYPEDEF_FLAG)
  add_definitions("-Wno-unused-local-typedef")
endif()
