# find gflags
find_path( GFlags_INCLUDE_DIRS gflags/gflags.h PATHS "/usr/include" "/usr/local/include" "/opt/local/include" )

find_library( GFlags_LIBRARIES NAMES gflags PATHS "/usr/lib" "/usr/local/lib" "/opt/local/lib" )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args( GFlags DEFAULT_MSG GFlags_INCLUDE_DIRS GFlags_LIBRARIES )

mark_as_advanced( GFlags_INCLUDE_DIRS GFlags_LIBRARIES )
