# AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1

## Introduction

Wandering environments is a fundamental task for developing every day activities. However, blind people have great difficulty for moving,
especially in unknown environments, which reduces their autonomy and puts them at risk of suffering an accident. We took techniques from
mobile robotics, autonomous cars and computer vision, and together with a high-performance and wearable computer and a stereo camera, we
implemented a system that assists blind people to wander unknown environments in a safe way, by sensing the world, segmenting the floor,
creating an occupancy 2D grid, reacting to close obstacles and generating vibration patterns with an haptic belt.

Considering the big amount of data that a stereo camera delivers, the fact that the person is moving in a highly dynamic
environment and the need of providing immediate feedback to the user, we defined two constraints: 1. Depth and color data
must be processed at real time, in a general purpose graphics processing unit GPGPU. 2. Images must be processed in a
wearable computer, it means, in a light and small processing device. Moreover, we defined trade-offs between sensing,
computation and system usability that allow the system fulfill these requirements appropriately.

The system shows an OpenGL window with the occupancy 2D grid. Besides the
OpenGL window, two OpenCV windows are shown: one for the color image and the other one for the depth image. This data, together with the positional tracking, is processed at real time and comes either from a stored SVO file or from the
stereo Camera ZED Mini. This source of data is selected by the user before executing the program.

In the upper left corner of the OpenGL window, information about the activation of the motors of the haptic belt is shown. If occupied cells are inside the semicircle around the user that is wearing the
system, the motors are activated (white point represents a deactivated motor while red point represents an activated motor). If occupied cells are in the region between 120 and 180 degrees, the
left motor is activated. If occupied cells are in the region between 60 and 120 degrees,
the middle motor is activated. If occupied cells are in the region between 0 and 60 degrees, the right motor is activated. The connection, using serial communication, with a real haptic belt is optional
and the system will run with or without this belt.

For avoiding wrong estimations of the plane, RANSAC is applied only if the number of points resulting from the initial segmentation (activePoints) is greater than a threshold (th_aPoints=90000). A low value of activePoints
happens when the user is close to an obstacle and the floor is not visible. In these cases, the equation of the plane that was computed with enought activePoints in the closest pose
 to the current one, is used for applying the transformations. This is the main difference of version 1.0 with respect to version 1.1.


## Installation

The Jetson TX2 has the dual-core Nvidia Denver2 + quad-core ARM Cortex-A57, a graphics processing unit for speeding up the data processing: the Nvidia Pascal with 256 cores and
 8GB of memory, and `Ubuntu 18.04` as operting system. `JetPack SDK 4.3` is used to flash the Jetson TX2 developer kit with the operating system image, and to install developer tools and libraries. It installs
`OpenCV 4.1` and `Cuda 10`. The library of the ZED Camera, the `ZED SDK 3.2.2` for JetPack 4.3, is used for capturing depth data, color data, and positional tracking. OpenCV
is employed for visualizing depth and color images and for carrying out basic
 operations with images. `OpenGL (freeGLUT)` allows the system to draw the occupancy 2D grid. Most of data processing is carried out
 on GPU, using Cuda 10, achieving to speed up the performance and to process data at real time. The activation of the motors of an haptic belt are represented
 as points (in white if a motor is deactivated and in red if a motor is activated) on the upper left corner of the window of OpenGL.
The system can optionally communicate with the control box of an haptic belt (version 1 and 2 of the belt) that has
vibrating motors, using the library [RS-232](https://www.teuniz.net/RS-232/), in order to send motion commands to the user. 
To use this library you need to put the `rs232.c` file in the src folder and the `rs232.h` file in include folder.

You can build and execute the project with Qtcreator by following the next steps (recommended in host computer).

1. Save the project "AssistNavBP, A VISION-BASED SYSTEM FOR ASSISTING BLIND PEOPLE TO WANDER UNKNOWN ENVIRONMENTS 1.1".
2. Install the libraries: ZED, OpenCV, OpenGL, Cuda, Thurst, Gflags, Glogs. Follow the instructions given by each  library. Remember that OpenCV, Cuda and Thrust are installed with JetPack, in both host computer and developer kit.
3. Open QtCreator. Click on "Open project" in the "Welcome" screen. Browse the project and select the CMakeLists.txt file located  in the main folder (first level).
4. Choose a location for the folder where the project will be built.
5. Run the CMake. If the libraries are found, Click on "Finish".
6. The configuration of the project and the modules required for its normal performance is carried out automatically, using the CMakeLists file, located in the main folder of the project (first level), and the FindGFlags and FindGLog files, located in the cmake folder. If some library is not found, manual configuration must be done.
 
Optionally, you can build the project without Qtcreator (recommended in Jetson TX2) by creating a build folder, running cmake and make, as follows.

```
cd <folder of the project>
sudo mkdir build
cd build
sudo cmake ..
sudo make
```

Next, in the build forder, you can execute the application by typing: sudo ./AssistNavBP

## Configuration

The main file is `src/main.cpp`. It defines the order of execution of the functions. It makes instances of the class `kernel_pc` and
  calls the most important functions for initializing vairables, allocating host and device memory, loading/capturing depth, RGB images and positional tracking,
computing local vertices, computing global vertices and normal vectors, making an initial floor segmentation, refining the initial segmentation with RANSAC, building an occupancy 2D grid and
carrying out reactive navigation. RANSAC is carried out for each frame if the number of activePoints is over a threshold. Moreover, it manages the functions and events for visualizing the occupancy 2D grid,
 the images captured with the camera and the communication with the haptic belt (if it is connected).
 
 The general setting of the project can be done using `Gflags` through command line (before executing the program).
 The parameters are listed next.
 
  - g_load_svo (true by default), a boolean variable. If true then a SVO file defined by the string svo_input_path is loaded. Otherwise, the Camera ZED Mini is used
  - g_svo_input_path ("/usr/local/zed/sample/svoFiles/prueba1.svo" by default), a string that defines the address and name of a svo file.
  - g_size_m_x (20 by default), a double variable that defines the size of the grid in x direction, in [m]
  - g_size_m_z (20 by default), a double variable that defines the size of the grid in z direction (y axis is parallel to gravity vector), in [m]
  - g_cell_m (0.1 by default), a doble variable for setting the long of a squared cell that belongs to the grid, in [m]
  - g_th_obst_mag (0.8 by default), a double variable for setting the threshold in distance that defines if an obstacle is close to the user, generating risk of collision, in [m]
  - g_max_iter_ransac (104 by default), an integer variable that defines the maximum  number of iterations for RANSAC
  - g_th_ransac (0.06 by default), a double variable for setting the threshold for RANSAC, that defines if a point belong to an equation plane or not, in [m]
  - g_th_occupancy_grid (0.2 by default), a double variable that defines the threshold for defining if a grid is free or occupied by evaluating the y component of the points, in [m].
  - g_downsample_scale (1 by default), an integer variable that defines the scale for downsampling the images
  
  You can copy the following text in command line and modify it according to the requirements.
  
  ```
  sudo /<adress of the build folder of the project>/AssistNavBP
   --g_load_svo true --g_svo_input_path /<adress of the folder where the svo files are located>/svofile.svo
   --g_size_m_x 20 --g_size_m_z 20 --g_cell_m 0.1 --g_th_obst_mag 0.8
   --g_max_iter_ransac 104 --g_th_ransac 0.06 --g_th_occupancy_grid 0.2 --g_downsample_scale 1
 ```
 
 You can see the description of the flags by typing in command line: sudo ./AssistNavBP --helpon main. Moreover, you can set only some flags. The remaining flags will be set automatically
to the default value defined in the `main.cpp` file.

Other option with Gflags is to edit and execute (sudo sh AssistNavBP.launch) a lauch file,
located in the launch folder and that contains the previous parameters.

There are other events that work when the OpenCV windows, used for showing the color and depth images, are activated. The "e" key finishes the while loop and the "q" key finishes the loop
 for visualizing the OpenGL window and the main program finishes. It is an alternative to "ESC" key to finish the main program but it works when any of the OpenCV window is activated
 
 Other parameters can be modified directly from the code (before executing the program):
 
  - Parameters in `main.cpp` for setting the serial communication, using the RS-232 library: port (c_port_nr, 16 for ttyS0 by default), baudrate (bdrate, 9600 by default),  communication mode (mode, '8','N','1',0 by default).
 - Parameters in `main.cpp` for setting the stereo camera ZED Mini: resolution (camera_resolution, sl::RESOLUTION_VGA (height 376,width 672) by default), frames per second (camera_fps, 15 fps by default),   cordinate system (coordinate_system, sl::COORDINATE_SYSTEM_IMAGE by default), depth mode (depth_mode, sl::DEPTH_MODE_QUALITY by default),   units of the coordinates (coordinate_units, sl::UNIT_METER by defaut), minimum distance in depth (depth_minimum_distance, 0.8 by default), maximum distance in depth  (setDepthMaxRangeValue, 4 by default), sensing mode (sensing_mode, sl::SENSING_MODE_STANDARD by default).
 - Parameters in `kernel_pc.cu` for avoiding outliers and carrying out the initial segmentation of the point cloud: threshold that defines if a point is an outlier by evaluating its y coordinate, in m   (th_outlier_y, 3.0 by default), threshold that defines if a normalized normal vector is parallel to the gravity vector (th_normal_y, 0.8 by default), threshold in y coordinate that  defines if a point is close to the floor (0.8 by default), minimum distance over which the occupancy 2D grid is updated, in m (min_dis_user, 0.4 by default).
  
## Getting Started
 
The application can be executed using either Qtcreator or the terminal (see Configuration section). If the parameter load_svo=true then the file
located in the path svo_input_path will be loaded and the images will be processed for computing the occupancy 2D grid. The svo file contains in compressed way, depth, color and positional data, captured
previously. The grid is built incrementaly as the user moves and covers more space. When the application is executed with the parameter
load_svo=false then the camera ZED Mini must be connected to the laptop/Jetson TX2 in order to begin to capture data and to process it at real time. The camera must point to the floor and be located on the
chest of the user. In both cases, either with svo file or with connected camera, three windows will appear on screen: an OpenGL window
for showing the occupancy 2D grid, an OpenCV window for visualizing the color image, and an OpenCV window for visualizing the depth image. The activation of the
motors will be visualized on the upper left corner of the OpenGL window. If an haptic belt is used (optional), then the motors will vibrate according to the reactive navigation, executed by the system.
 
The authors are not responsible in any manner for any injure or accident which may occur by testing the system in real environments and with people carrying out tasks that
involve motion.  
  
  
## Authors
 - Andres Diaz, Ph.D. Universidad Nacional Abierta y a Distancia UNAD
 - Sixto Campaña, Ph.D. Universidad Nacional Abierta y a Distancia UNAD
 - Eduardo Caicedo, Ph.D. Universidad del Valle